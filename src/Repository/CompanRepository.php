<?php

namespace App\Repository;

use App\Entity\Compan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Compan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compan[]    findAll()
 * @method Compan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compan::class);
    }

    // /**
    //  * @return Compan[] Returns an array of Compan objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Compan
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
