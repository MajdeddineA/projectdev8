<?php

namespace App\Repository;

use App\Entity\Compayis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Compayis|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compayis|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compayis[]    findAll()
 * @method Compayis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompayisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compayis::class);
    }

    // /**
    //  * @return Compayis[] Returns an array of Compayis objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Compayis
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
