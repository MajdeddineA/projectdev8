<?php

namespace App\Repository;

use App\Entity\Compaies;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Compaies|null find($id, $lockMode = null, $lockVersion = null)
 * @method Compaies|null findOneBy(array $criteria, array $orderBy = null)
 * @method Compaies[]    findAll()
 * @method Compaies[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompaiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Compaies::class);
    }

    // /**
    //  * @return Compaies[] Returns an array of Compaies objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Compaies
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
