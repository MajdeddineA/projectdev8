<?php

namespace App\Entity;

use App\Repository\CompanRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanRepository::class)
 */
class Compan
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=225)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=225)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=225, nullable=true)
     */
    private $sirt;

    /**
     * @ORM\OneToMany(targetEntity=Person::class, mappedBy="compan")
     */
    private $persons;

    public function __construct()
    {
        $this->persons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getSirt(): ?string
    {
        return $this->sirt;
    }

    public function setSirt(?string $sirt): self
    {
        $this->sirt = $sirt;

        return $this;
    }

    /**
     * @return Collection|Person[]
     */
    public function getPersons(): Collection
    {
        return $this->persons;
    }

    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
            $person->setCompan($this);
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
            // set the owning side to null (unless already changed)
            if ($person->getCompan() === $this) {
                $person->setCompan(null);
            }
        }

        return $this;
    }
}
