<?php

namespace App\Controller;

use App\Entity\Compan;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CompanyController extends AbstractController
{
    /**
     * @Route("/company/new", name="new_company")
     */
    public function newCompany(EntityManagerInterface $entityManarger)
    {
        $compan = new Compan();
        $compan ->setName('Thomas');
        $compan ->setAddress('Simplon');
        $compan ->setSirt('12345');

        $entityManarger->persist($compan);
        $entityManarger->flush();


        return $this->render('company/index.html.twig');
    }


    /**
     * @Route("/company/list", name="listCompany")
     */
    public function listCompany(EntityManagerInterface $entityManarger)
    {
        $companyRepository = $entityManarger->getRepository(Compan::class);

        $arrayCompanies = $companyRepository->findAll();

        //dump($arrayCompanies);exit();

        return $this->render('company/indexx.html.twig', [
            'companies' => $arrayCompanies
        ]);
    }


    /**
     * @Route("/company/{id}", name="showCompany")
     */
    public function showCompany(EntityManagerInterface $entityManarger, $id)
    {
        $companyRepository = $entityManarger->getRepository(Compan::class);

        $company = $companyRepository->find($id);

        //dump($arrayCompanies);exit();

        return $this->render('company/show.html.twig', [
            'company' => $company
        ]);
    }


    /**
     * @Route("/company/edit/{id}", name="editCompany")
     */
    public function editCompany(EntityManagerInterface $entityManarger, $id)
    {
        $companyRepository = $entityManarger->getRepository(Compan::class);

        $company = $companyRepository->find($id);

        $company->setName('sdsd');
        $company->setSirt('234454545');

        $entityManarger->flush();

        return $this->render('company/show.html.twig', [
            'company' => $company
        ]);
    }


    /**
     * @Route("/company/delete/{id}", name="deleteCompany")
     */
    public function deleteCompany(EntityManagerInterface $entityManarger, $id)
    {
        $companyRepository = $entityManarger->getRepository(Compan::class);

        $company = $companyRepository->find($id);

        $entityManarger->remove($company);

        $entityManarger->flush();

        return $this->render('company/show.html.twig', [
            'company' => $company
        ]);
    }

}
