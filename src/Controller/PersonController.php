<?php

namespace App\Controller;

use App\Entity\Compan;
use App\Entity\Company;
use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PersonController extends AbstractController
{
    /**
     * @Route("/person/new", name="newPerson")
     */
    public function newPerson(EntityManagerInterface $entityManarger)
    {

        $compan = new Compan();
        $compan->setName('Thomas');
        $compan->setAddress('sdsads');
        $compan->setSirt('23434353');

        $company = new Company();
        $company->setName('NN');
        $company->setAddress('dgfgf');
        $company->setSirt('354554');

        $person = new Person();
        $person->setNom('Najd');
        $person->setPrenom('Alhafez');
        $person->setEmail('aadsds@gmail.com');
        $person->setAge(30);
        $person->setCompan($compan);
        $person->setCompany($company);

        $entityManarger->persist($company);
        $entityManarger->persist($compan);
        $entityManarger->persist($person);
        $entityManarger->flush();
        
        return $this->render('person/index.html.twig');
    }

    /**
     * @Route("/person/list", name="listPerson")
     */
    public function listPerson(EntityManagerInterface $entityManarger)
    {
        $personRepository = $entityManarger->getRepository(Person::class);

        $arrayPersons = $personRepository->findAll();

        //dump($arrayPersons);exit();

        return $this->render('person/list.html.twig', [
            'persons' => $arrayPersons
        ]);

    }

    /**
     * @Route("/person/{id}", name="showPerson")
     */
    public function showPerson(EntityManagerInterface $entityManarger, $id)
    {
        $personRepository = $entityManarger->getRepository(Person::class);

        $persons = $personRepository->find($id);

        //dump($arrayCompanies);exit();

        return $this->render('person/show.html.twig', [
            'person' => $persons
        ]);
    }

    /**
     * @Route("/person/edit/{id}", name="editPerson")
     */
    public function editPerson(EntityManagerInterface $entityManarger, $id)
    {
        $personRepository = $entityManarger->getRepository(Person::class);

        $person = $personRepository->find($id);

        $person->setNom('sdsd');
        $person->setPrenom('sew');

        $entityManarger->flush();

        return $this->render('person/show.html.twig', [
            'person' => $person
        ]);
    }

    /**
     * @Route("/person/delete/{id}", name="deletePerson")
     */
    public function deletePerson(EntityManagerInterface $entityManarger, $id)
    {
        $personRepository = $entityManarger->getRepository(Person::class);

        $person = $personRepository->find($id);

        $entityManarger->remove($person);

        $entityManarger->flush();

        return $this->render('person/show.html.twig', [
            'person' => $person
        ]);
    }
}
