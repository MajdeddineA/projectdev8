<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController
{
    /**
     * @Route("home")
     */
    public function home()
    {
        //return new Response("Hello");
        return $this->render("Home.html.twig");
        
    }

    /**
     * @Route("base")
     */
    public function base()
    {
        //return new Response("Hello");
        return $this->render("base.html.twig");
        
    }

     /**
     * @Route("base/contact")
     */
    public function contactsimplon()
    {
        return $this->render("baseContact.html.twig");
        
    }

    /**
     * @Route("base/apropos")
     */
    public function apropos()
    {
        return $this->render("baseApropos.html.twig");
        
    }

    /**
     * @Route("base/plus")
     */
    public function plus()
    {
        return $this->render("baseplus.html.twig");
        
    }

    /**
     * @Route("base/pluss")
     */
    public function pluss()
    {
        return $this->render("basepluss.html.twig");
        
    }

    /**
     * @Route("home/contact")
     */
    public function contact()
    {
        //return new Response("Hello");
        return $this->render("contact.html.twig");
    }

    
    /**
     * @Route("home/getcontact/{prenom}", name="getcontact", methods={"GET"})
     */
    public function getcontact($prenom)
    {
        //return new Response("Hello");
        //$prenom = "Alhafez";
        echo $prenom;
        return $this->render("contact.html.twig", [
            'prenom' => $prenom
        ]);
    }

    /**
     * @Route("plus/{a}/{b}",
     *     name="sum",
     *     requirements={"a"="\d+", "b"="\d+"},
     *     methods={"GET"}
     *     )
     */
    public function sum($a, $b)
    {
        echo $a, $b;
        return $this->render('Home.html.twig', [
            'sum' => $a +$b
        ]);
    }


    /**
     * @Route("pluss/{a}/{b}",
     *     name="summ",
     *     requirements={"a"="\d+", "b"="\d+"},
     *     methods={"GET"}
     *     )
     */
    public function summ(int $a, int $b)
    {
        return $this->json(['sum' => $a +$b
        ]);
    }

}