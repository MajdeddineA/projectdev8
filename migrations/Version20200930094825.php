<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200930094825 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE compan (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(225) NOT NULL, address VARCHAR(225) NOT NULL, sirt VARCHAR(225) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE person ADD compan_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1768DC4F9D3 FOREIGN KEY (compan_id) REFERENCES compan (id)');
        $this->addSql('CREATE INDEX IDX_34DCD1768DC4F9D3 ON person (compan_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD1768DC4F9D3');
        $this->addSql('DROP TABLE compan');
        $this->addSql('DROP INDEX IDX_34DCD1768DC4F9D3 ON person');
        $this->addSql('ALTER TABLE person DROP compan_id');
    }
}
