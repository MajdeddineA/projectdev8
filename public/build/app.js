(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/css/app.css":
/*!****************************!*\
  !*** ./assets/css/app.css ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _css_app_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../css/app.css */ "./assets/css/app.css");
/* harmony import */ var _css_app_css__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_app_css__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _scss_style_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../scss/style.scss */ "./assets/scss/style.scss");
/* harmony import */ var _scss_style_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_scss_style_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _js_script_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../js/script.js */ "./assets/js/script.js");
/* harmony import */ var _js_script_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_js_script_js__WEBPACK_IMPORTED_MODULE_2__);
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you import will output into a single css file (app.css in this case)


 // Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js"); // this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything


__webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js"); // or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');


$(document).ready(function () {
  $('[data-toggle="popover"]').popover();
});
console.log('Hello Webpack Encore! Edit me in assets/js/app.js');

/***/ }),

/***/ "./assets/js/script.js":
/*!*****************************!*\
  !*** ./assets/js/script.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {__webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");

__webpack_require__(/*! core-js/modules/es.array.index-of */ "./node_modules/core-js/modules/es.array.index-of.js");

__webpack_require__(/*! core-js/modules/es.date.to-string */ "./node_modules/core-js/modules/es.date.to-string.js");

__webpack_require__(/*! core-js/modules/es.number.constructor */ "./node_modules/core-js/modules/es.number.constructor.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");

__webpack_require__(/*! core-js/modules/es.string.split */ "./node_modules/core-js/modules/es.string.split.js");

//console.log('dddddddd');
global.$ = global.jQuery = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
jQuery(document).ready(function ($) {
  //console.log('dddddddd');
  var timelines = $('.cd-horizontal-timeline'),
      eventsMinDistance = 60;
  timelines.length > 0 && initTimeline(timelines);

  function initTimeline(timelines) {
    timelines.each(function () {
      var timeline = $(this),
          timelineComponents = {}; //cache timeline components 

      timelineComponents['timelineWrapper'] = timeline.find('.events-wrapper');
      timelineComponents['eventsWrapper'] = timelineComponents['timelineWrapper'].children('.events');
      timelineComponents['fillingLine'] = timelineComponents['eventsWrapper'].children('.filling-line');
      timelineComponents['timelineEvents'] = timelineComponents['eventsWrapper'].find('a');
      timelineComponents['timelineDates'] = parseDate(timelineComponents['timelineEvents']);
      timelineComponents['eventsMinLapse'] = minLapse(timelineComponents['timelineDates']);
      timelineComponents['timelineNavigation'] = timeline.find('.cd-timeline-navigation');
      timelineComponents['eventsContent'] = timeline.children('.events-content'); //assign a left postion to the single events along the timeline

      setDatePosition(timelineComponents, eventsMinDistance); //assign a width to the timeline

      var timelineTotWidth = setTimelineWidth(timelineComponents, eventsMinDistance); //the timeline has been initialize - show it

      timeline.addClass('loaded'); //detect click on the next arrow

      timelineComponents['timelineNavigation'].on('click', '.next', function (event) {
        event.preventDefault();
        updateSlide(timelineComponents, timelineTotWidth, 'next');
      }); //detect click on the prev arrow

      timelineComponents['timelineNavigation'].on('click', '.prev', function (event) {
        event.preventDefault();
        updateSlide(timelineComponents, timelineTotWidth, 'prev');
      }); //detect click on the a single event - show new event content

      timelineComponents['eventsWrapper'].on('click', 'a', function (event) {
        event.preventDefault();
        timelineComponents['timelineEvents'].removeClass('selected');
        $(this).addClass('selected');
        updateOlderEvents($(this));
        updateFilling($(this), timelineComponents['fillingLine'], timelineTotWidth);
        updateVisibleContent($(this), timelineComponents['eventsContent']);
      }); //on swipe, show next/prev event content

      timelineComponents['eventsContent'].on('swipeleft', function () {
        var mq = checkMQ();
        mq == 'mobile' && showNewContent(timelineComponents, timelineTotWidth, 'next');
      });
      timelineComponents['eventsContent'].on('swiperight', function () {
        var mq = checkMQ();
        mq == 'mobile' && showNewContent(timelineComponents, timelineTotWidth, 'prev');
      }); //keyboard navigation

      $(document).keyup(function (event) {
        if (event.which == '37' && elementInViewport(timeline.get(0))) {
          showNewContent(timelineComponents, timelineTotWidth, 'prev');
        } else if (event.which == '39' && elementInViewport(timeline.get(0))) {
          showNewContent(timelineComponents, timelineTotWidth, 'next');
        }
      });
    });
  }

  function updateSlide(timelineComponents, timelineTotWidth, string) {
    //retrieve translateX value of timelineComponents['eventsWrapper']
    var translateValue = getTranslateValue(timelineComponents['eventsWrapper']),
        wrapperWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', '')); //translate the timeline to the left('next')/right('prev') 

    string == 'next' ? translateTimeline(timelineComponents, translateValue - wrapperWidth + eventsMinDistance, wrapperWidth - timelineTotWidth) : translateTimeline(timelineComponents, translateValue + wrapperWidth - eventsMinDistance);
  }

  function showNewContent(timelineComponents, timelineTotWidth, string) {
    //go from one event to the next/previous one
    var visibleContent = timelineComponents['eventsContent'].find('.selected'),
        newContent = string == 'next' ? visibleContent.next() : visibleContent.prev();

    if (newContent.length > 0) {
      //if there's a next/prev event - show it
      var selectedDate = timelineComponents['eventsWrapper'].find('.selected'),
          newEvent = string == 'next' ? selectedDate.parent('li').next('li').children('a') : selectedDate.parent('li').prev('li').children('a');
      updateFilling(newEvent, timelineComponents['fillingLine'], timelineTotWidth);
      updateVisibleContent(newEvent, timelineComponents['eventsContent']);
      newEvent.addClass('selected');
      selectedDate.removeClass('selected');
      updateOlderEvents(newEvent);
      updateTimelinePosition(string, newEvent, timelineComponents);
    }
  }

  function updateTimelinePosition(string, event, timelineComponents) {
    //translate timeline to the left/right according to the position of the selected event
    var eventStyle = window.getComputedStyle(event.get(0), null),
        eventLeft = Number(eventStyle.getPropertyValue("left").replace('px', '')),
        timelineWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', '')),
        timelineTotWidth = Number(timelineComponents['eventsWrapper'].css('width').replace('px', ''));
    var timelineTranslate = getTranslateValue(timelineComponents['eventsWrapper']);

    if (string == 'next' && eventLeft > timelineWidth - timelineTranslate || string == 'prev' && eventLeft < -timelineTranslate) {
      translateTimeline(timelineComponents, -eventLeft + timelineWidth / 2, timelineWidth - timelineTotWidth);
    }
  }

  function translateTimeline(timelineComponents, value, totWidth) {
    var eventsWrapper = timelineComponents['eventsWrapper'].get(0);
    value = value > 0 ? 0 : value; //only negative translate value

    value = !(typeof totWidth === 'undefined') && value < totWidth ? totWidth : value; //do not translate more than timeline width

    setTransformValue(eventsWrapper, 'translateX', value + 'px'); //update navigation arrows visibility

    value == 0 ? timelineComponents['timelineNavigation'].find('.prev').addClass('inactive') : timelineComponents['timelineNavigation'].find('.prev').removeClass('inactive');
    value == totWidth ? timelineComponents['timelineNavigation'].find('.next').addClass('inactive') : timelineComponents['timelineNavigation'].find('.next').removeClass('inactive');
  }

  function updateFilling(selectedEvent, filling, totWidth) {
    //change .filling-line length according to the selected event
    var eventStyle = window.getComputedStyle(selectedEvent.get(0), null),
        eventLeft = eventStyle.getPropertyValue("left"),
        eventWidth = eventStyle.getPropertyValue("width");
    eventLeft = Number(eventLeft.replace('px', '')) + Number(eventWidth.replace('px', '')) / 2;
    var scaleValue = eventLeft / totWidth;
    setTransformValue(filling.get(0), 'scaleX', scaleValue);
  }

  function setDatePosition(timelineComponents, min) {
    for (i = 0; i < timelineComponents['timelineDates'].length; i++) {
      var distance = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][i]),
          distanceNorm = Math.round(distance / timelineComponents['eventsMinLapse']) + 2;
      timelineComponents['timelineEvents'].eq(i).css('left', distanceNorm * min + 'px');
    }
  }

  function setTimelineWidth(timelineComponents, width) {
    var timeSpan = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][timelineComponents['timelineDates'].length - 1]),
        timeSpanNorm = timeSpan / timelineComponents['eventsMinLapse'],
        timeSpanNorm = Math.round(timeSpanNorm) + 4,
        totalWidth = timeSpanNorm * width;
    timelineComponents['eventsWrapper'].css('width', totalWidth + 'px');
    updateFilling(timelineComponents['eventsWrapper'].find('a.selected'), timelineComponents['fillingLine'], totalWidth);
    updateTimelinePosition('next', timelineComponents['eventsWrapper'].find('a.selected'), timelineComponents);
    return totalWidth;
  }

  function updateVisibleContent(event, eventsContent) {
    var eventDate = event.data('date'),
        visibleContent = eventsContent.find('.selected'),
        selectedContent = eventsContent.find('[data-date="' + eventDate + '"]'),
        selectedContentHeight = selectedContent.height();

    if (selectedContent.index() > visibleContent.index()) {
      var classEnetering = 'selected enter-right',
          classLeaving = 'leave-left';
    } else {
      var classEnetering = 'selected enter-left',
          classLeaving = 'leave-right';
    }

    selectedContent.attr('class', classEnetering);
    visibleContent.attr('class', classLeaving).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function () {
      visibleContent.removeClass('leave-right leave-left');
      selectedContent.removeClass('enter-left enter-right');
    });
    eventsContent.css('height', selectedContentHeight + 'px');
  }

  function updateOlderEvents(event) {
    event.parent('li').prevAll('li').children('a').addClass('older-event').end().end().nextAll('li').children('a').removeClass('older-event');
  }

  function getTranslateValue(timeline) {
    var timelineStyle = window.getComputedStyle(timeline.get(0), null),
        timelineTranslate = timelineStyle.getPropertyValue("-webkit-transform") || timelineStyle.getPropertyValue("-moz-transform") || timelineStyle.getPropertyValue("-ms-transform") || timelineStyle.getPropertyValue("-o-transform") || timelineStyle.getPropertyValue("transform");

    if (timelineTranslate.indexOf('(') >= 0) {
      var timelineTranslate = timelineTranslate.split('(')[1];
      timelineTranslate = timelineTranslate.split(')')[0];
      timelineTranslate = timelineTranslate.split(',');
      var translateValue = timelineTranslate[4];
    } else {
      var translateValue = 0;
    }

    return Number(translateValue);
  }

  function setTransformValue(element, property, value) {
    element.style["-webkit-transform"] = property + "(" + value + ")";
    element.style["-moz-transform"] = property + "(" + value + ")";
    element.style["-ms-transform"] = property + "(" + value + ")";
    element.style["-o-transform"] = property + "(" + value + ")";
    element.style["transform"] = property + "(" + value + ")";
  } //based on http://stackoverflow.com/questions/542938/how-do-i-get-the-number-of-days-between-two-dates-in-javascript


  function parseDate(events) {
    var dateArrays = [];
    events.each(function () {
      var singleDate = $(this),
          dateComp = singleDate.data('date').split('T');

      if (dateComp.length > 1) {
        //both DD/MM/YEAR and time are provided
        var dayComp = dateComp[0].split('/'),
            timeComp = dateComp[1].split(':');
      } else if (dateComp[0].indexOf(':') >= 0) {
        //only time is provide
        var dayComp = ["2000", "0", "0"],
            timeComp = dateComp[0].split(':');
      } else {
        //only DD/MM/YEAR
        var dayComp = dateComp[0].split('/'),
            timeComp = ["0", "0"];
      }

      var newDate = new Date(dayComp[2], dayComp[1] - 1, dayComp[0], timeComp[0], timeComp[1]);
      dateArrays.push(newDate);
    });
    return dateArrays;
  }

  function daydiff(first, second) {
    return Math.round(second - first);
  }

  function minLapse(dates) {
    //determine the minimum distance among events
    var dateDistances = [];

    for (i = 1; i < dates.length; i++) {
      var distance = daydiff(dates[i - 1], dates[i]);
      dateDistances.push(distance);
    }

    return Math.min.apply(null, dateDistances);
  }
  /*
  	How to tell if a DOM element is visible in the current viewport?
  	http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
  */


  function elementInViewport(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while (el.offsetParent) {
      el = el.offsetParent;
      top += el.offsetTop;
      left += el.offsetLeft;
    }

    return top < window.pageYOffset + window.innerHeight && left < window.pageXOffset + window.innerWidth && top + height > window.pageYOffset && left + width > window.pageXOffset;
  }

  function checkMQ() {
    //check if mobile or desktop device
    return window.getComputedStyle(document.querySelector('.cd-horizontal-timeline'), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
  }
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./assets/scss/style.scss":
/*!********************************!*\
  !*** ./assets/scss/style.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/js/app.js","runtime","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL2FwcC5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvc2NyaXB0LmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9zY3NzL3N0eWxlLnNjc3MiXSwibmFtZXMiOlsiJCIsInJlcXVpcmUiLCJkb2N1bWVudCIsInJlYWR5IiwicG9wb3ZlciIsImNvbnNvbGUiLCJsb2ciLCJnbG9iYWwiLCJqUXVlcnkiLCJ0aW1lbGluZXMiLCJldmVudHNNaW5EaXN0YW5jZSIsImxlbmd0aCIsImluaXRUaW1lbGluZSIsImVhY2giLCJ0aW1lbGluZSIsInRpbWVsaW5lQ29tcG9uZW50cyIsImZpbmQiLCJjaGlsZHJlbiIsInBhcnNlRGF0ZSIsIm1pbkxhcHNlIiwic2V0RGF0ZVBvc2l0aW9uIiwidGltZWxpbmVUb3RXaWR0aCIsInNldFRpbWVsaW5lV2lkdGgiLCJhZGRDbGFzcyIsIm9uIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsInVwZGF0ZVNsaWRlIiwicmVtb3ZlQ2xhc3MiLCJ1cGRhdGVPbGRlckV2ZW50cyIsInVwZGF0ZUZpbGxpbmciLCJ1cGRhdGVWaXNpYmxlQ29udGVudCIsIm1xIiwiY2hlY2tNUSIsInNob3dOZXdDb250ZW50Iiwia2V5dXAiLCJ3aGljaCIsImVsZW1lbnRJblZpZXdwb3J0IiwiZ2V0Iiwic3RyaW5nIiwidHJhbnNsYXRlVmFsdWUiLCJnZXRUcmFuc2xhdGVWYWx1ZSIsIndyYXBwZXJXaWR0aCIsIk51bWJlciIsImNzcyIsInJlcGxhY2UiLCJ0cmFuc2xhdGVUaW1lbGluZSIsInZpc2libGVDb250ZW50IiwibmV3Q29udGVudCIsIm5leHQiLCJwcmV2Iiwic2VsZWN0ZWREYXRlIiwibmV3RXZlbnQiLCJwYXJlbnQiLCJ1cGRhdGVUaW1lbGluZVBvc2l0aW9uIiwiZXZlbnRTdHlsZSIsIndpbmRvdyIsImdldENvbXB1dGVkU3R5bGUiLCJldmVudExlZnQiLCJnZXRQcm9wZXJ0eVZhbHVlIiwidGltZWxpbmVXaWR0aCIsInRpbWVsaW5lVHJhbnNsYXRlIiwidmFsdWUiLCJ0b3RXaWR0aCIsImV2ZW50c1dyYXBwZXIiLCJzZXRUcmFuc2Zvcm1WYWx1ZSIsInNlbGVjdGVkRXZlbnQiLCJmaWxsaW5nIiwiZXZlbnRXaWR0aCIsInNjYWxlVmFsdWUiLCJtaW4iLCJpIiwiZGlzdGFuY2UiLCJkYXlkaWZmIiwiZGlzdGFuY2VOb3JtIiwiTWF0aCIsInJvdW5kIiwiZXEiLCJ3aWR0aCIsInRpbWVTcGFuIiwidGltZVNwYW5Ob3JtIiwidG90YWxXaWR0aCIsImV2ZW50c0NvbnRlbnQiLCJldmVudERhdGUiLCJkYXRhIiwic2VsZWN0ZWRDb250ZW50Iiwic2VsZWN0ZWRDb250ZW50SGVpZ2h0IiwiaGVpZ2h0IiwiaW5kZXgiLCJjbGFzc0VuZXRlcmluZyIsImNsYXNzTGVhdmluZyIsImF0dHIiLCJvbmUiLCJwcmV2QWxsIiwiZW5kIiwibmV4dEFsbCIsInRpbWVsaW5lU3R5bGUiLCJpbmRleE9mIiwic3BsaXQiLCJlbGVtZW50IiwicHJvcGVydHkiLCJzdHlsZSIsImV2ZW50cyIsImRhdGVBcnJheXMiLCJzaW5nbGVEYXRlIiwiZGF0ZUNvbXAiLCJkYXlDb21wIiwidGltZUNvbXAiLCJuZXdEYXRlIiwiRGF0ZSIsInB1c2giLCJmaXJzdCIsInNlY29uZCIsImRhdGVzIiwiZGF0ZURpc3RhbmNlcyIsImFwcGx5IiwiZWwiLCJ0b3AiLCJvZmZzZXRUb3AiLCJsZWZ0Iiwib2Zmc2V0TGVmdCIsIm9mZnNldFdpZHRoIiwib2Zmc2V0SGVpZ2h0Iiwib2Zmc2V0UGFyZW50IiwicGFnZVlPZmZzZXQiLCJpbm5lckhlaWdodCIsInBhZ2VYT2Zmc2V0IiwiaW5uZXJXaWR0aCIsInF1ZXJ5U2VsZWN0b3IiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLHVDOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7QUFPQTtBQUNBO0FBQ0E7Q0FHQTtBQUNBOztBQUVBLElBQU1BLENBQUMsR0FBR0MsbUJBQU8sQ0FBQyxvREFBRCxDQUFqQixDLENBQ0E7QUFDQTs7O0FBQ0FBLG1CQUFPLENBQUMsZ0VBQUQsQ0FBUCxDLENBRUE7QUFDQTtBQUNBOzs7QUFFQUQsQ0FBQyxDQUFDRSxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFXO0FBQ3pCSCxHQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2QkksT0FBN0I7QUFDSCxDQUZEO0FBSUFDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLG1EQUFaLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1QkE7QUFDQUMsTUFBTSxDQUFDUCxDQUFQLEdBQVdPLE1BQU0sQ0FBQ0MsTUFBUCxHQUFnQlAsbUJBQU8sQ0FBQyxvREFBRCxDQUFsQztBQUNBTyxNQUFNLENBQUNOLFFBQUQsQ0FBTixDQUFpQkMsS0FBakIsQ0FBdUIsVUFBU0gsQ0FBVCxFQUFXO0FBQzlCO0FBQ0EsTUFBSVMsU0FBUyxHQUFHVCxDQUFDLENBQUMseUJBQUQsQ0FBakI7QUFBQSxNQUNGVSxpQkFBaUIsR0FBRyxFQURsQjtBQUdGRCxXQUFTLENBQUNFLE1BQVYsR0FBbUIsQ0FBcEIsSUFBMEJDLFlBQVksQ0FBQ0gsU0FBRCxDQUF0Qzs7QUFFQSxXQUFTRyxZQUFULENBQXNCSCxTQUF0QixFQUFpQztBQUNoQ0EsYUFBUyxDQUFDSSxJQUFWLENBQWUsWUFBVTtBQUN4QixVQUFJQyxRQUFRLEdBQUdkLENBQUMsQ0FBQyxJQUFELENBQWhCO0FBQUEsVUFDQ2Usa0JBQWtCLEdBQUcsRUFEdEIsQ0FEd0IsQ0FHeEI7O0FBQ0FBLHdCQUFrQixDQUFDLGlCQUFELENBQWxCLEdBQXdDRCxRQUFRLENBQUNFLElBQVQsQ0FBYyxpQkFBZCxDQUF4QztBQUNBRCx3QkFBa0IsQ0FBQyxlQUFELENBQWxCLEdBQXNDQSxrQkFBa0IsQ0FBQyxpQkFBRCxDQUFsQixDQUFzQ0UsUUFBdEMsQ0FBK0MsU0FBL0MsQ0FBdEM7QUFDQUYsd0JBQWtCLENBQUMsYUFBRCxDQUFsQixHQUFvQ0Esa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ0UsUUFBcEMsQ0FBNkMsZUFBN0MsQ0FBcEM7QUFDQUYsd0JBQWtCLENBQUMsZ0JBQUQsQ0FBbEIsR0FBdUNBLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0NDLElBQXBDLENBQXlDLEdBQXpDLENBQXZDO0FBQ0FELHdCQUFrQixDQUFDLGVBQUQsQ0FBbEIsR0FBc0NHLFNBQVMsQ0FBQ0gsa0JBQWtCLENBQUMsZ0JBQUQsQ0FBbkIsQ0FBL0M7QUFDQUEsd0JBQWtCLENBQUMsZ0JBQUQsQ0FBbEIsR0FBdUNJLFFBQVEsQ0FBQ0osa0JBQWtCLENBQUMsZUFBRCxDQUFuQixDQUEvQztBQUNBQSx3QkFBa0IsQ0FBQyxvQkFBRCxDQUFsQixHQUEyQ0QsUUFBUSxDQUFDRSxJQUFULENBQWMseUJBQWQsQ0FBM0M7QUFDQUQsd0JBQWtCLENBQUMsZUFBRCxDQUFsQixHQUFzQ0QsUUFBUSxDQUFDRyxRQUFULENBQWtCLGlCQUFsQixDQUF0QyxDQVh3QixDQWF4Qjs7QUFDQUcscUJBQWUsQ0FBQ0wsa0JBQUQsRUFBcUJMLGlCQUFyQixDQUFmLENBZHdCLENBZXhCOztBQUNBLFVBQUlXLGdCQUFnQixHQUFHQyxnQkFBZ0IsQ0FBQ1Asa0JBQUQsRUFBcUJMLGlCQUFyQixDQUF2QyxDQWhCd0IsQ0FpQnhCOztBQUNBSSxjQUFRLENBQUNTLFFBQVQsQ0FBa0IsUUFBbEIsRUFsQndCLENBb0J4Qjs7QUFDQVIsd0JBQWtCLENBQUMsb0JBQUQsQ0FBbEIsQ0FBeUNTLEVBQXpDLENBQTRDLE9BQTVDLEVBQXFELE9BQXJELEVBQThELFVBQVNDLEtBQVQsRUFBZTtBQUM1RUEsYUFBSyxDQUFDQyxjQUFOO0FBQ0FDLG1CQUFXLENBQUNaLGtCQUFELEVBQXFCTSxnQkFBckIsRUFBdUMsTUFBdkMsQ0FBWDtBQUNBLE9BSEQsRUFyQndCLENBeUJ4Qjs7QUFDQU4sd0JBQWtCLENBQUMsb0JBQUQsQ0FBbEIsQ0FBeUNTLEVBQXpDLENBQTRDLE9BQTVDLEVBQXFELE9BQXJELEVBQThELFVBQVNDLEtBQVQsRUFBZTtBQUM1RUEsYUFBSyxDQUFDQyxjQUFOO0FBQ0FDLG1CQUFXLENBQUNaLGtCQUFELEVBQXFCTSxnQkFBckIsRUFBdUMsTUFBdkMsQ0FBWDtBQUNBLE9BSEQsRUExQndCLENBOEJ4Qjs7QUFDQU4sd0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ1MsRUFBcEMsQ0FBdUMsT0FBdkMsRUFBZ0QsR0FBaEQsRUFBcUQsVUFBU0MsS0FBVCxFQUFlO0FBQ25FQSxhQUFLLENBQUNDLGNBQU47QUFDQVgsMEJBQWtCLENBQUMsZ0JBQUQsQ0FBbEIsQ0FBcUNhLFdBQXJDLENBQWlELFVBQWpEO0FBQ0E1QixTQUFDLENBQUMsSUFBRCxDQUFELENBQVF1QixRQUFSLENBQWlCLFVBQWpCO0FBQ0FNLHlCQUFpQixDQUFDN0IsQ0FBQyxDQUFDLElBQUQsQ0FBRixDQUFqQjtBQUNBOEIscUJBQWEsQ0FBQzlCLENBQUMsQ0FBQyxJQUFELENBQUYsRUFBVWUsa0JBQWtCLENBQUMsYUFBRCxDQUE1QixFQUE2Q00sZ0JBQTdDLENBQWI7QUFDQVUsNEJBQW9CLENBQUMvQixDQUFDLENBQUMsSUFBRCxDQUFGLEVBQVVlLGtCQUFrQixDQUFDLGVBQUQsQ0FBNUIsQ0FBcEI7QUFDQSxPQVBELEVBL0J3QixDQXdDeEI7O0FBQ0FBLHdCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0NTLEVBQXBDLENBQXVDLFdBQXZDLEVBQW9ELFlBQVU7QUFDN0QsWUFBSVEsRUFBRSxHQUFHQyxPQUFPLEVBQWhCO0FBQ0VELFVBQUUsSUFBSSxRQUFSLElBQXNCRSxjQUFjLENBQUNuQixrQkFBRCxFQUFxQk0sZ0JBQXJCLEVBQXVDLE1BQXZDLENBQXBDO0FBQ0EsT0FIRDtBQUlBTix3QkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DUyxFQUFwQyxDQUF1QyxZQUF2QyxFQUFxRCxZQUFVO0FBQzlELFlBQUlRLEVBQUUsR0FBR0MsT0FBTyxFQUFoQjtBQUNFRCxVQUFFLElBQUksUUFBUixJQUFzQkUsY0FBYyxDQUFDbkIsa0JBQUQsRUFBcUJNLGdCQUFyQixFQUF1QyxNQUF2QyxDQUFwQztBQUNBLE9BSEQsRUE3Q3dCLENBa0R4Qjs7QUFDQXJCLE9BQUMsQ0FBQ0UsUUFBRCxDQUFELENBQVlpQyxLQUFaLENBQWtCLFVBQVNWLEtBQVQsRUFBZTtBQUNoQyxZQUFHQSxLQUFLLENBQUNXLEtBQU4sSUFBYSxJQUFiLElBQXFCQyxpQkFBaUIsQ0FBQ3ZCLFFBQVEsQ0FBQ3dCLEdBQVQsQ0FBYSxDQUFiLENBQUQsQ0FBekMsRUFBNkQ7QUFDNURKLHdCQUFjLENBQUNuQixrQkFBRCxFQUFxQk0sZ0JBQXJCLEVBQXVDLE1BQXZDLENBQWQ7QUFDQSxTQUZELE1BRU8sSUFBSUksS0FBSyxDQUFDVyxLQUFOLElBQWEsSUFBYixJQUFxQkMsaUJBQWlCLENBQUN2QixRQUFRLENBQUN3QixHQUFULENBQWEsQ0FBYixDQUFELENBQTFDLEVBQTZEO0FBQ25FSix3QkFBYyxDQUFDbkIsa0JBQUQsRUFBcUJNLGdCQUFyQixFQUF1QyxNQUF2QyxDQUFkO0FBQ0E7QUFDRCxPQU5EO0FBT0EsS0ExREQ7QUEyREE7O0FBRUQsV0FBU00sV0FBVCxDQUFxQlosa0JBQXJCLEVBQXlDTSxnQkFBekMsRUFBMkRrQixNQUEzRCxFQUFtRTtBQUNsRTtBQUNBLFFBQUlDLGNBQWMsR0FBR0MsaUJBQWlCLENBQUMxQixrQkFBa0IsQ0FBQyxlQUFELENBQW5CLENBQXRDO0FBQUEsUUFDQzJCLFlBQVksR0FBR0MsTUFBTSxDQUFDNUIsa0JBQWtCLENBQUMsaUJBQUQsQ0FBbEIsQ0FBc0M2QixHQUF0QyxDQUEwQyxPQUExQyxFQUFtREMsT0FBbkQsQ0FBMkQsSUFBM0QsRUFBaUUsRUFBakUsQ0FBRCxDQUR0QixDQUZrRSxDQUlsRTs7QUFDQ04sVUFBTSxJQUFJLE1BQVgsR0FDR08saUJBQWlCLENBQUMvQixrQkFBRCxFQUFxQnlCLGNBQWMsR0FBR0UsWUFBakIsR0FBZ0NoQyxpQkFBckQsRUFBd0VnQyxZQUFZLEdBQUdyQixnQkFBdkYsQ0FEcEIsR0FFR3lCLGlCQUFpQixDQUFDL0Isa0JBQUQsRUFBcUJ5QixjQUFjLEdBQUdFLFlBQWpCLEdBQWdDaEMsaUJBQXJELENBRnBCO0FBR0E7O0FBRUQsV0FBU3dCLGNBQVQsQ0FBd0JuQixrQkFBeEIsRUFBNENNLGdCQUE1QyxFQUE4RGtCLE1BQTlELEVBQXNFO0FBQ3JFO0FBQ0EsUUFBSVEsY0FBYyxHQUFJaEMsa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ0MsSUFBcEMsQ0FBeUMsV0FBekMsQ0FBdEI7QUFBQSxRQUNDZ0MsVUFBVSxHQUFLVCxNQUFNLElBQUksTUFBWixHQUF1QlEsY0FBYyxDQUFDRSxJQUFmLEVBQXZCLEdBQStDRixjQUFjLENBQUNHLElBQWYsRUFEN0Q7O0FBR0EsUUFBS0YsVUFBVSxDQUFDckMsTUFBWCxHQUFvQixDQUF6QixFQUE2QjtBQUFFO0FBQzlCLFVBQUl3QyxZQUFZLEdBQUdwQyxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DQyxJQUFwQyxDQUF5QyxXQUF6QyxDQUFuQjtBQUFBLFVBQ0NvQyxRQUFRLEdBQUtiLE1BQU0sSUFBSSxNQUFaLEdBQXVCWSxZQUFZLENBQUNFLE1BQWIsQ0FBb0IsSUFBcEIsRUFBMEJKLElBQTFCLENBQStCLElBQS9CLEVBQXFDaEMsUUFBckMsQ0FBOEMsR0FBOUMsQ0FBdkIsR0FBNEVrQyxZQUFZLENBQUNFLE1BQWIsQ0FBb0IsSUFBcEIsRUFBMEJILElBQTFCLENBQStCLElBQS9CLEVBQXFDakMsUUFBckMsQ0FBOEMsR0FBOUMsQ0FEeEY7QUFHQWEsbUJBQWEsQ0FBQ3NCLFFBQUQsRUFBV3JDLGtCQUFrQixDQUFDLGFBQUQsQ0FBN0IsRUFBOENNLGdCQUE5QyxDQUFiO0FBQ0FVLDBCQUFvQixDQUFDcUIsUUFBRCxFQUFXckMsa0JBQWtCLENBQUMsZUFBRCxDQUE3QixDQUFwQjtBQUNBcUMsY0FBUSxDQUFDN0IsUUFBVCxDQUFrQixVQUFsQjtBQUNBNEIsa0JBQVksQ0FBQ3ZCLFdBQWIsQ0FBeUIsVUFBekI7QUFDQUMsdUJBQWlCLENBQUN1QixRQUFELENBQWpCO0FBQ0FFLDRCQUFzQixDQUFDZixNQUFELEVBQVNhLFFBQVQsRUFBbUJyQyxrQkFBbkIsQ0FBdEI7QUFDQTtBQUNEOztBQUVELFdBQVN1QyxzQkFBVCxDQUFnQ2YsTUFBaEMsRUFBd0NkLEtBQXhDLEVBQStDVixrQkFBL0MsRUFBbUU7QUFDbEU7QUFDQSxRQUFJd0MsVUFBVSxHQUFHQyxNQUFNLENBQUNDLGdCQUFQLENBQXdCaEMsS0FBSyxDQUFDYSxHQUFOLENBQVUsQ0FBVixDQUF4QixFQUFzQyxJQUF0QyxDQUFqQjtBQUFBLFFBQ0NvQixTQUFTLEdBQUdmLE1BQU0sQ0FBQ1ksVUFBVSxDQUFDSSxnQkFBWCxDQUE0QixNQUE1QixFQUFvQ2QsT0FBcEMsQ0FBNEMsSUFBNUMsRUFBa0QsRUFBbEQsQ0FBRCxDQURuQjtBQUFBLFFBRUNlLGFBQWEsR0FBR2pCLE1BQU0sQ0FBQzVCLGtCQUFrQixDQUFDLGlCQUFELENBQWxCLENBQXNDNkIsR0FBdEMsQ0FBMEMsT0FBMUMsRUFBbURDLE9BQW5ELENBQTJELElBQTNELEVBQWlFLEVBQWpFLENBQUQsQ0FGdkI7QUFBQSxRQUdDeEIsZ0JBQWdCLEdBQUdzQixNQUFNLENBQUM1QixrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DNkIsR0FBcEMsQ0FBd0MsT0FBeEMsRUFBaURDLE9BQWpELENBQXlELElBQXpELEVBQStELEVBQS9ELENBQUQsQ0FIMUI7QUFJQSxRQUFJZ0IsaUJBQWlCLEdBQUdwQixpQkFBaUIsQ0FBQzFCLGtCQUFrQixDQUFDLGVBQUQsQ0FBbkIsQ0FBekM7O0FBRU0sUUFBS3dCLE1BQU0sSUFBSSxNQUFWLElBQW9CbUIsU0FBUyxHQUFHRSxhQUFhLEdBQUdDLGlCQUFqRCxJQUF3RXRCLE1BQU0sSUFBSSxNQUFWLElBQW9CbUIsU0FBUyxHQUFHLENBQUVHLGlCQUE5RyxFQUFtSTtBQUNsSWYsdUJBQWlCLENBQUMvQixrQkFBRCxFQUFxQixDQUFFMkMsU0FBRixHQUFjRSxhQUFhLEdBQUMsQ0FBakQsRUFBb0RBLGFBQWEsR0FBR3ZDLGdCQUFwRSxDQUFqQjtBQUNBO0FBQ1A7O0FBRUQsV0FBU3lCLGlCQUFULENBQTJCL0Isa0JBQTNCLEVBQStDK0MsS0FBL0MsRUFBc0RDLFFBQXRELEVBQWdFO0FBQy9ELFFBQUlDLGFBQWEsR0FBR2pELGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0N1QixHQUFwQyxDQUF3QyxDQUF4QyxDQUFwQjtBQUNBd0IsU0FBSyxHQUFJQSxLQUFLLEdBQUcsQ0FBVCxHQUFjLENBQWQsR0FBa0JBLEtBQTFCLENBRitELENBRTlCOztBQUNqQ0EsU0FBSyxHQUFLLEVBQUUsT0FBT0MsUUFBUCxLQUFvQixXQUF0QixLQUF1Q0QsS0FBSyxHQUFHQyxRQUFqRCxHQUE4REEsUUFBOUQsR0FBeUVELEtBQWpGLENBSCtELENBR3lCOztBQUN4RkcscUJBQWlCLENBQUNELGFBQUQsRUFBZ0IsWUFBaEIsRUFBOEJGLEtBQUssR0FBQyxJQUFwQyxDQUFqQixDQUorRCxDQUsvRDs7QUFDQ0EsU0FBSyxJQUFJLENBQVYsR0FBZ0IvQyxrQkFBa0IsQ0FBQyxvQkFBRCxDQUFsQixDQUF5Q0MsSUFBekMsQ0FBOEMsT0FBOUMsRUFBdURPLFFBQXZELENBQWdFLFVBQWhFLENBQWhCLEdBQThGUixrQkFBa0IsQ0FBQyxvQkFBRCxDQUFsQixDQUF5Q0MsSUFBekMsQ0FBOEMsT0FBOUMsRUFBdURZLFdBQXZELENBQW1FLFVBQW5FLENBQTlGO0FBQ0NrQyxTQUFLLElBQUlDLFFBQVYsR0FBdUJoRCxrQkFBa0IsQ0FBQyxvQkFBRCxDQUFsQixDQUF5Q0MsSUFBekMsQ0FBOEMsT0FBOUMsRUFBdURPLFFBQXZELENBQWdFLFVBQWhFLENBQXZCLEdBQXFHUixrQkFBa0IsQ0FBQyxvQkFBRCxDQUFsQixDQUF5Q0MsSUFBekMsQ0FBOEMsT0FBOUMsRUFBdURZLFdBQXZELENBQW1FLFVBQW5FLENBQXJHO0FBQ0E7O0FBRUQsV0FBU0UsYUFBVCxDQUF1Qm9DLGFBQXZCLEVBQXNDQyxPQUF0QyxFQUErQ0osUUFBL0MsRUFBeUQ7QUFDeEQ7QUFDQSxRQUFJUixVQUFVLEdBQUdDLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0JTLGFBQWEsQ0FBQzVCLEdBQWQsQ0FBa0IsQ0FBbEIsQ0FBeEIsRUFBOEMsSUFBOUMsQ0FBakI7QUFBQSxRQUNDb0IsU0FBUyxHQUFHSCxVQUFVLENBQUNJLGdCQUFYLENBQTRCLE1BQTVCLENBRGI7QUFBQSxRQUVDUyxVQUFVLEdBQUdiLFVBQVUsQ0FBQ0ksZ0JBQVgsQ0FBNEIsT0FBNUIsQ0FGZDtBQUdBRCxhQUFTLEdBQUdmLE1BQU0sQ0FBQ2UsU0FBUyxDQUFDYixPQUFWLENBQWtCLElBQWxCLEVBQXdCLEVBQXhCLENBQUQsQ0FBTixHQUFzQ0YsTUFBTSxDQUFDeUIsVUFBVSxDQUFDdkIsT0FBWCxDQUFtQixJQUFuQixFQUF5QixFQUF6QixDQUFELENBQU4sR0FBcUMsQ0FBdkY7QUFDQSxRQUFJd0IsVUFBVSxHQUFHWCxTQUFTLEdBQUNLLFFBQTNCO0FBQ0FFLHFCQUFpQixDQUFDRSxPQUFPLENBQUM3QixHQUFSLENBQVksQ0FBWixDQUFELEVBQWlCLFFBQWpCLEVBQTJCK0IsVUFBM0IsQ0FBakI7QUFDQTs7QUFFRCxXQUFTakQsZUFBVCxDQUF5Qkwsa0JBQXpCLEVBQTZDdUQsR0FBN0MsRUFBa0Q7QUFDakQsU0FBS0MsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHeEQsa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ0osTUFBcEQsRUFBNEQ0RCxDQUFDLEVBQTdELEVBQWlFO0FBQzdELFVBQUlDLFFBQVEsR0FBR0MsT0FBTyxDQUFDMUQsa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQyxDQUFwQyxDQUFELEVBQXlDQSxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9Dd0QsQ0FBcEMsQ0FBekMsQ0FBdEI7QUFBQSxVQUNDRyxZQUFZLEdBQUdDLElBQUksQ0FBQ0MsS0FBTCxDQUFXSixRQUFRLEdBQUN6RCxrQkFBa0IsQ0FBQyxnQkFBRCxDQUF0QyxJQUE0RCxDQUQ1RTtBQUVBQSx3QkFBa0IsQ0FBQyxnQkFBRCxDQUFsQixDQUFxQzhELEVBQXJDLENBQXdDTixDQUF4QyxFQUEyQzNCLEdBQTNDLENBQStDLE1BQS9DLEVBQXVEOEIsWUFBWSxHQUFDSixHQUFiLEdBQWlCLElBQXhFO0FBQ0g7QUFDRDs7QUFFRCxXQUFTaEQsZ0JBQVQsQ0FBMEJQLGtCQUExQixFQUE4QytELEtBQTlDLEVBQXFEO0FBQ3BELFFBQUlDLFFBQVEsR0FBR04sT0FBTyxDQUFDMUQsa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQyxDQUFwQyxDQUFELEVBQXlDQSxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DQSxrQkFBa0IsQ0FBQyxlQUFELENBQWxCLENBQW9DSixNQUFwQyxHQUEyQyxDQUEvRSxDQUF6QyxDQUF0QjtBQUFBLFFBQ0NxRSxZQUFZLEdBQUdELFFBQVEsR0FBQ2hFLGtCQUFrQixDQUFDLGdCQUFELENBRDNDO0FBQUEsUUFFQ2lFLFlBQVksR0FBR0wsSUFBSSxDQUFDQyxLQUFMLENBQVdJLFlBQVgsSUFBMkIsQ0FGM0M7QUFBQSxRQUdDQyxVQUFVLEdBQUdELFlBQVksR0FBQ0YsS0FIM0I7QUFJQS9ELHNCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0M2QixHQUFwQyxDQUF3QyxPQUF4QyxFQUFpRHFDLFVBQVUsR0FBQyxJQUE1RDtBQUNBbkQsaUJBQWEsQ0FBQ2Ysa0JBQWtCLENBQUMsZUFBRCxDQUFsQixDQUFvQ0MsSUFBcEMsQ0FBeUMsWUFBekMsQ0FBRCxFQUF5REQsa0JBQWtCLENBQUMsYUFBRCxDQUEzRSxFQUE0RmtFLFVBQTVGLENBQWI7QUFDQTNCLDBCQUFzQixDQUFDLE1BQUQsRUFBU3ZDLGtCQUFrQixDQUFDLGVBQUQsQ0FBbEIsQ0FBb0NDLElBQXBDLENBQXlDLFlBQXpDLENBQVQsRUFBaUVELGtCQUFqRSxDQUF0QjtBQUVBLFdBQU9rRSxVQUFQO0FBQ0E7O0FBRUQsV0FBU2xELG9CQUFULENBQThCTixLQUE5QixFQUFxQ3lELGFBQXJDLEVBQW9EO0FBQ25ELFFBQUlDLFNBQVMsR0FBRzFELEtBQUssQ0FBQzJELElBQU4sQ0FBVyxNQUFYLENBQWhCO0FBQUEsUUFDQ3JDLGNBQWMsR0FBR21DLGFBQWEsQ0FBQ2xFLElBQWQsQ0FBbUIsV0FBbkIsQ0FEbEI7QUFBQSxRQUVDcUUsZUFBZSxHQUFHSCxhQUFhLENBQUNsRSxJQUFkLENBQW1CLGlCQUFnQm1FLFNBQWhCLEdBQTJCLElBQTlDLENBRm5CO0FBQUEsUUFHQ0cscUJBQXFCLEdBQUdELGVBQWUsQ0FBQ0UsTUFBaEIsRUFIekI7O0FBS0EsUUFBSUYsZUFBZSxDQUFDRyxLQUFoQixLQUEwQnpDLGNBQWMsQ0FBQ3lDLEtBQWYsRUFBOUIsRUFBc0Q7QUFDckQsVUFBSUMsY0FBYyxHQUFHLHNCQUFyQjtBQUFBLFVBQ0NDLFlBQVksR0FBRyxZQURoQjtBQUVBLEtBSEQsTUFHTztBQUNOLFVBQUlELGNBQWMsR0FBRyxxQkFBckI7QUFBQSxVQUNDQyxZQUFZLEdBQUcsYUFEaEI7QUFFQTs7QUFFREwsbUJBQWUsQ0FBQ00sSUFBaEIsQ0FBcUIsT0FBckIsRUFBOEJGLGNBQTlCO0FBQ0ExQyxrQkFBYyxDQUFDNEMsSUFBZixDQUFvQixPQUFwQixFQUE2QkQsWUFBN0IsRUFBMkNFLEdBQTNDLENBQStDLDhEQUEvQyxFQUErRyxZQUFVO0FBQ3hIN0Msb0JBQWMsQ0FBQ25CLFdBQWYsQ0FBMkIsd0JBQTNCO0FBQ0F5RCxxQkFBZSxDQUFDekQsV0FBaEIsQ0FBNEIsd0JBQTVCO0FBQ0EsS0FIRDtBQUlBc0QsaUJBQWEsQ0FBQ3RDLEdBQWQsQ0FBa0IsUUFBbEIsRUFBNEIwQyxxQkFBcUIsR0FBQyxJQUFsRDtBQUNBOztBQUVELFdBQVN6RCxpQkFBVCxDQUEyQkosS0FBM0IsRUFBa0M7QUFDakNBLFNBQUssQ0FBQzRCLE1BQU4sQ0FBYSxJQUFiLEVBQW1Cd0MsT0FBbkIsQ0FBMkIsSUFBM0IsRUFBaUM1RSxRQUFqQyxDQUEwQyxHQUExQyxFQUErQ00sUUFBL0MsQ0FBd0QsYUFBeEQsRUFBdUV1RSxHQUF2RSxHQUE2RUEsR0FBN0UsR0FBbUZDLE9BQW5GLENBQTJGLElBQTNGLEVBQWlHOUUsUUFBakcsQ0FBMEcsR0FBMUcsRUFBK0dXLFdBQS9HLENBQTJILGFBQTNIO0FBQ0E7O0FBRUQsV0FBU2EsaUJBQVQsQ0FBMkIzQixRQUEzQixFQUFxQztBQUNwQyxRQUFJa0YsYUFBYSxHQUFHeEMsTUFBTSxDQUFDQyxnQkFBUCxDQUF3QjNDLFFBQVEsQ0FBQ3dCLEdBQVQsQ0FBYSxDQUFiLENBQXhCLEVBQXlDLElBQXpDLENBQXBCO0FBQUEsUUFDQ3VCLGlCQUFpQixHQUFHbUMsYUFBYSxDQUFDckMsZ0JBQWQsQ0FBK0IsbUJBQS9CLEtBQ1pxQyxhQUFhLENBQUNyQyxnQkFBZCxDQUErQixnQkFBL0IsQ0FEWSxJQUVacUMsYUFBYSxDQUFDckMsZ0JBQWQsQ0FBK0IsZUFBL0IsQ0FGWSxJQUdacUMsYUFBYSxDQUFDckMsZ0JBQWQsQ0FBK0IsY0FBL0IsQ0FIWSxJQUlacUMsYUFBYSxDQUFDckMsZ0JBQWQsQ0FBK0IsV0FBL0IsQ0FMVDs7QUFPTSxRQUFJRSxpQkFBaUIsQ0FBQ29DLE9BQWxCLENBQTBCLEdBQTFCLEtBQWlDLENBQXJDLEVBQXlDO0FBQ3hDLFVBQUlwQyxpQkFBaUIsR0FBR0EsaUJBQWlCLENBQUNxQyxLQUFsQixDQUF3QixHQUF4QixFQUE2QixDQUE3QixDQUF4QjtBQUNIckMsdUJBQWlCLEdBQUdBLGlCQUFpQixDQUFDcUMsS0FBbEIsQ0FBd0IsR0FBeEIsRUFBNkIsQ0FBN0IsQ0FBcEI7QUFDQXJDLHVCQUFpQixHQUFHQSxpQkFBaUIsQ0FBQ3FDLEtBQWxCLENBQXdCLEdBQXhCLENBQXBCO0FBQ0EsVUFBSTFELGNBQWMsR0FBR3FCLGlCQUFpQixDQUFDLENBQUQsQ0FBdEM7QUFDRyxLQUxELE1BS087QUFDTixVQUFJckIsY0FBYyxHQUFHLENBQXJCO0FBQ0E7O0FBRUQsV0FBT0csTUFBTSxDQUFDSCxjQUFELENBQWI7QUFDTjs7QUFFRCxXQUFTeUIsaUJBQVQsQ0FBMkJrQyxPQUEzQixFQUFvQ0MsUUFBcEMsRUFBOEN0QyxLQUE5QyxFQUFxRDtBQUNwRHFDLFdBQU8sQ0FBQ0UsS0FBUixDQUFjLG1CQUFkLElBQXFDRCxRQUFRLEdBQUMsR0FBVCxHQUFhdEMsS0FBYixHQUFtQixHQUF4RDtBQUNBcUMsV0FBTyxDQUFDRSxLQUFSLENBQWMsZ0JBQWQsSUFBa0NELFFBQVEsR0FBQyxHQUFULEdBQWF0QyxLQUFiLEdBQW1CLEdBQXJEO0FBQ0FxQyxXQUFPLENBQUNFLEtBQVIsQ0FBYyxlQUFkLElBQWlDRCxRQUFRLEdBQUMsR0FBVCxHQUFhdEMsS0FBYixHQUFtQixHQUFwRDtBQUNBcUMsV0FBTyxDQUFDRSxLQUFSLENBQWMsY0FBZCxJQUFnQ0QsUUFBUSxHQUFDLEdBQVQsR0FBYXRDLEtBQWIsR0FBbUIsR0FBbkQ7QUFDQXFDLFdBQU8sQ0FBQ0UsS0FBUixDQUFjLFdBQWQsSUFBNkJELFFBQVEsR0FBQyxHQUFULEdBQWF0QyxLQUFiLEdBQW1CLEdBQWhEO0FBQ0EsR0ExTWdDLENBNE1qQzs7O0FBQ0EsV0FBUzVDLFNBQVQsQ0FBbUJvRixNQUFuQixFQUEyQjtBQUMxQixRQUFJQyxVQUFVLEdBQUcsRUFBakI7QUFDQUQsVUFBTSxDQUFDekYsSUFBUCxDQUFZLFlBQVU7QUFDckIsVUFBSTJGLFVBQVUsR0FBR3hHLENBQUMsQ0FBQyxJQUFELENBQWxCO0FBQUEsVUFDQ3lHLFFBQVEsR0FBR0QsVUFBVSxDQUFDcEIsSUFBWCxDQUFnQixNQUFoQixFQUF3QmMsS0FBeEIsQ0FBOEIsR0FBOUIsQ0FEWjs7QUFFQSxVQUFJTyxRQUFRLENBQUM5RixNQUFULEdBQWtCLENBQXRCLEVBQTBCO0FBQUU7QUFDM0IsWUFBSStGLE9BQU8sR0FBR0QsUUFBUSxDQUFDLENBQUQsQ0FBUixDQUFZUCxLQUFaLENBQWtCLEdBQWxCLENBQWQ7QUFBQSxZQUNDUyxRQUFRLEdBQUdGLFFBQVEsQ0FBQyxDQUFELENBQVIsQ0FBWVAsS0FBWixDQUFrQixHQUFsQixDQURaO0FBRUEsT0FIRCxNQUdPLElBQUlPLFFBQVEsQ0FBQyxDQUFELENBQVIsQ0FBWVIsT0FBWixDQUFvQixHQUFwQixLQUEyQixDQUEvQixFQUFtQztBQUFFO0FBQzNDLFlBQUlTLE9BQU8sR0FBRyxDQUFDLE1BQUQsRUFBUyxHQUFULEVBQWMsR0FBZCxDQUFkO0FBQUEsWUFDQ0MsUUFBUSxHQUFHRixRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVlQLEtBQVosQ0FBa0IsR0FBbEIsQ0FEWjtBQUVBLE9BSE0sTUFHQTtBQUFFO0FBQ1IsWUFBSVEsT0FBTyxHQUFHRCxRQUFRLENBQUMsQ0FBRCxDQUFSLENBQVlQLEtBQVosQ0FBa0IsR0FBbEIsQ0FBZDtBQUFBLFlBQ0NTLFFBQVEsR0FBRyxDQUFDLEdBQUQsRUFBTSxHQUFOLENBRFo7QUFFQTs7QUFDRCxVQUFJQyxPQUFPLEdBQUcsSUFBSUMsSUFBSixDQUFTSCxPQUFPLENBQUMsQ0FBRCxDQUFoQixFQUFxQkEsT0FBTyxDQUFDLENBQUQsQ0FBUCxHQUFXLENBQWhDLEVBQW1DQSxPQUFPLENBQUMsQ0FBRCxDQUExQyxFQUErQ0MsUUFBUSxDQUFDLENBQUQsQ0FBdkQsRUFBNERBLFFBQVEsQ0FBQyxDQUFELENBQXBFLENBQWQ7QUFDQUosZ0JBQVUsQ0FBQ08sSUFBWCxDQUFnQkYsT0FBaEI7QUFDQSxLQWZEO0FBZ0JHLFdBQU9MLFVBQVA7QUFDSDs7QUFFRCxXQUFTOUIsT0FBVCxDQUFpQnNDLEtBQWpCLEVBQXdCQyxNQUF4QixFQUFnQztBQUM1QixXQUFPckMsSUFBSSxDQUFDQyxLQUFMLENBQVlvQyxNQUFNLEdBQUNELEtBQW5CLENBQVA7QUFDSDs7QUFFRCxXQUFTNUYsUUFBVCxDQUFrQjhGLEtBQWxCLEVBQXlCO0FBQ3hCO0FBQ0EsUUFBSUMsYUFBYSxHQUFHLEVBQXBCOztBQUNBLFNBQUszQyxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUcwQyxLQUFLLENBQUN0RyxNQUF0QixFQUE4QjRELENBQUMsRUFBL0IsRUFBbUM7QUFDL0IsVUFBSUMsUUFBUSxHQUFHQyxPQUFPLENBQUN3QyxLQUFLLENBQUMxQyxDQUFDLEdBQUMsQ0FBSCxDQUFOLEVBQWEwQyxLQUFLLENBQUMxQyxDQUFELENBQWxCLENBQXRCO0FBQ0EyQyxtQkFBYSxDQUFDSixJQUFkLENBQW1CdEMsUUFBbkI7QUFDSDs7QUFDRCxXQUFPRyxJQUFJLENBQUNMLEdBQUwsQ0FBUzZDLEtBQVQsQ0FBZSxJQUFmLEVBQXFCRCxhQUFyQixDQUFQO0FBQ0E7QUFFRDs7Ozs7O0FBSUEsV0FBUzdFLGlCQUFULENBQTJCK0UsRUFBM0IsRUFBK0I7QUFDOUIsUUFBSUMsR0FBRyxHQUFHRCxFQUFFLENBQUNFLFNBQWI7QUFDQSxRQUFJQyxJQUFJLEdBQUdILEVBQUUsQ0FBQ0ksVUFBZDtBQUNBLFFBQUkxQyxLQUFLLEdBQUdzQyxFQUFFLENBQUNLLFdBQWY7QUFDQSxRQUFJbEMsTUFBTSxHQUFHNkIsRUFBRSxDQUFDTSxZQUFoQjs7QUFFQSxXQUFNTixFQUFFLENBQUNPLFlBQVQsRUFBdUI7QUFDbkJQLFFBQUUsR0FBR0EsRUFBRSxDQUFDTyxZQUFSO0FBQ0FOLFNBQUcsSUFBSUQsRUFBRSxDQUFDRSxTQUFWO0FBQ0FDLFVBQUksSUFBSUgsRUFBRSxDQUFDSSxVQUFYO0FBQ0g7O0FBRUQsV0FDSUgsR0FBRyxHQUFJN0QsTUFBTSxDQUFDb0UsV0FBUCxHQUFxQnBFLE1BQU0sQ0FBQ3FFLFdBQW5DLElBQ0FOLElBQUksR0FBSS9ELE1BQU0sQ0FBQ3NFLFdBQVAsR0FBcUJ0RSxNQUFNLENBQUN1RSxVQURwQyxJQUVDVixHQUFHLEdBQUc5QixNQUFQLEdBQWlCL0IsTUFBTSxDQUFDb0UsV0FGeEIsSUFHQ0wsSUFBSSxHQUFHekMsS0FBUixHQUFpQnRCLE1BQU0sQ0FBQ3NFLFdBSjVCO0FBTUE7O0FBRUQsV0FBUzdGLE9BQVQsR0FBbUI7QUFDbEI7QUFDQSxXQUFPdUIsTUFBTSxDQUFDQyxnQkFBUCxDQUF3QnZELFFBQVEsQ0FBQzhILGFBQVQsQ0FBdUIseUJBQXZCLENBQXhCLEVBQTJFLFVBQTNFLEVBQXVGckUsZ0JBQXZGLENBQXdHLFNBQXhHLEVBQW1IZCxPQUFuSCxDQUEySCxJQUEzSCxFQUFpSSxFQUFqSSxFQUFxSUEsT0FBckksQ0FBNkksSUFBN0ksRUFBbUosRUFBbkosQ0FBUDtBQUNHO0FBQ0osQ0E1UUQsRTs7Ozs7Ozs7Ozs7O0FDRkEsdUMiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIiwiLypcbiAqIFdlbGNvbWUgdG8geW91ciBhcHAncyBtYWluIEphdmFTY3JpcHQgZmlsZSFcbiAqXG4gKiBXZSByZWNvbW1lbmQgaW5jbHVkaW5nIHRoZSBidWlsdCB2ZXJzaW9uIG9mIHRoaXMgSmF2YVNjcmlwdCBmaWxlXG4gKiAoYW5kIGl0cyBDU1MgZmlsZSkgaW4geW91ciBiYXNlIGxheW91dCAoYmFzZS5odG1sLnR3aWcpLlxuICovXG5cbi8vIGFueSBDU1MgeW91IGltcG9ydCB3aWxsIG91dHB1dCBpbnRvIGEgc2luZ2xlIGNzcyBmaWxlIChhcHAuY3NzIGluIHRoaXMgY2FzZSlcbmltcG9ydCAnLi4vY3NzL2FwcC5jc3MnO1xuaW1wb3J0ICcuLi9zY3NzL3N0eWxlLnNjc3MnO1xuaW1wb3J0ICcuLi9qcy9zY3JpcHQuanMnO1xuXG4vLyBOZWVkIGpRdWVyeT8gSW5zdGFsbCBpdCB3aXRoIFwieWFybiBhZGQganF1ZXJ5XCIsIHRoZW4gdW5jb21tZW50IHRvIGltcG9ydCBpdC5cbi8vIGltcG9ydCAkIGZyb20gJ2pxdWVyeSc7XG5cbmNvbnN0ICQgPSByZXF1aXJlKCdqcXVlcnknKTtcbi8vIHRoaXMgXCJtb2RpZmllc1wiIHRoZSBqcXVlcnkgbW9kdWxlOiBhZGRpbmcgYmVoYXZpb3IgdG8gaXRcbi8vIHRoZSBib290c3RyYXAgbW9kdWxlIGRvZXNuJ3QgZXhwb3J0L3JldHVybiBhbnl0aGluZ1xucmVxdWlyZSgnYm9vdHN0cmFwJyk7XG5cbi8vIG9yIHlvdSBjYW4gaW5jbHVkZSBzcGVjaWZpYyBwaWVjZXNcbi8vIHJlcXVpcmUoJ2Jvb3RzdHJhcC9qcy9kaXN0L3Rvb2x0aXAnKTtcbi8vIHJlcXVpcmUoJ2Jvb3RzdHJhcC9qcy9kaXN0L3BvcG92ZXInKTtcblxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJCgnW2RhdGEtdG9nZ2xlPVwicG9wb3ZlclwiXScpLnBvcG92ZXIoKTtcbn0pO1xuXG5jb25zb2xlLmxvZygnSGVsbG8gV2VicGFjayBFbmNvcmUhIEVkaXQgbWUgaW4gYXNzZXRzL2pzL2FwcC5qcycpOyIsIi8vY29uc29sZS5sb2coJ2RkZGRkZGRkJyk7XG5nbG9iYWwuJCA9IGdsb2JhbC5qUXVlcnkgPSByZXF1aXJlKCdqcXVlcnknKTtcbmpRdWVyeShkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oJCl7XG4gICAgLy9jb25zb2xlLmxvZygnZGRkZGRkZGQnKTtcbiAgICB2YXIgdGltZWxpbmVzID0gJCgnLmNkLWhvcml6b250YWwtdGltZWxpbmUnKSxcblx0XHRldmVudHNNaW5EaXN0YW5jZSA9IDYwO1xuXG5cdCh0aW1lbGluZXMubGVuZ3RoID4gMCkgJiYgaW5pdFRpbWVsaW5lKHRpbWVsaW5lcyk7XG5cblx0ZnVuY3Rpb24gaW5pdFRpbWVsaW5lKHRpbWVsaW5lcykge1xuXHRcdHRpbWVsaW5lcy5lYWNoKGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgdGltZWxpbmUgPSAkKHRoaXMpLFxuXHRcdFx0XHR0aW1lbGluZUNvbXBvbmVudHMgPSB7fTtcblx0XHRcdC8vY2FjaGUgdGltZWxpbmUgY29tcG9uZW50cyBcblx0XHRcdHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVXcmFwcGVyJ10gPSB0aW1lbGluZS5maW5kKCcuZXZlbnRzLXdyYXBwZXInKTtcblx0XHRcdHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddID0gdGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZVdyYXBwZXInXS5jaGlsZHJlbignLmV2ZW50cycpO1xuXHRcdFx0dGltZWxpbmVDb21wb25lbnRzWydmaWxsaW5nTGluZSddID0gdGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uY2hpbGRyZW4oJy5maWxsaW5nLWxpbmUnKTtcblx0XHRcdHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVFdmVudHMnXSA9IHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddLmZpbmQoJ2EnKTtcblx0XHRcdHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddID0gcGFyc2VEYXRlKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVFdmVudHMnXSk7XG5cdFx0XHR0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c01pbkxhcHNlJ10gPSBtaW5MYXBzZSh0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRGF0ZXMnXSk7XG5cdFx0XHR0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lTmF2aWdhdGlvbiddID0gdGltZWxpbmUuZmluZCgnLmNkLXRpbWVsaW5lLW5hdmlnYXRpb24nKTtcblx0XHRcdHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzQ29udGVudCddID0gdGltZWxpbmUuY2hpbGRyZW4oJy5ldmVudHMtY29udGVudCcpO1xuXG5cdFx0XHQvL2Fzc2lnbiBhIGxlZnQgcG9zdGlvbiB0byB0aGUgc2luZ2xlIGV2ZW50cyBhbG9uZyB0aGUgdGltZWxpbmVcblx0XHRcdHNldERhdGVQb3NpdGlvbih0aW1lbGluZUNvbXBvbmVudHMsIGV2ZW50c01pbkRpc3RhbmNlKTtcblx0XHRcdC8vYXNzaWduIGEgd2lkdGggdG8gdGhlIHRpbWVsaW5lXG5cdFx0XHR2YXIgdGltZWxpbmVUb3RXaWR0aCA9IHNldFRpbWVsaW5lV2lkdGgodGltZWxpbmVDb21wb25lbnRzLCBldmVudHNNaW5EaXN0YW5jZSk7XG5cdFx0XHQvL3RoZSB0aW1lbGluZSBoYXMgYmVlbiBpbml0aWFsaXplIC0gc2hvdyBpdFxuXHRcdFx0dGltZWxpbmUuYWRkQ2xhc3MoJ2xvYWRlZCcpO1xuXG5cdFx0XHQvL2RldGVjdCBjbGljayBvbiB0aGUgbmV4dCBhcnJvd1xuXHRcdFx0dGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXS5vbignY2xpY2snLCAnLm5leHQnLCBmdW5jdGlvbihldmVudCl7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdHVwZGF0ZVNsaWRlKHRpbWVsaW5lQ29tcG9uZW50cywgdGltZWxpbmVUb3RXaWR0aCwgJ25leHQnKTtcblx0XHRcdH0pO1xuXHRcdFx0Ly9kZXRlY3QgY2xpY2sgb24gdGhlIHByZXYgYXJyb3dcblx0XHRcdHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVOYXZpZ2F0aW9uJ10ub24oJ2NsaWNrJywgJy5wcmV2JywgZnVuY3Rpb24oZXZlbnQpe1xuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHR1cGRhdGVTbGlkZSh0aW1lbGluZUNvbXBvbmVudHMsIHRpbWVsaW5lVG90V2lkdGgsICdwcmV2Jyk7XG5cdFx0XHR9KTtcblx0XHRcdC8vZGV0ZWN0IGNsaWNrIG9uIHRoZSBhIHNpbmdsZSBldmVudCAtIHNob3cgbmV3IGV2ZW50IGNvbnRlbnRcblx0XHRcdHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddLm9uKCdjbGljaycsICdhJywgZnVuY3Rpb24oZXZlbnQpe1xuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHR0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRXZlbnRzJ10ucmVtb3ZlQ2xhc3MoJ3NlbGVjdGVkJyk7XG5cdFx0XHRcdCQodGhpcykuYWRkQ2xhc3MoJ3NlbGVjdGVkJyk7XG5cdFx0XHRcdHVwZGF0ZU9sZGVyRXZlbnRzKCQodGhpcykpO1xuXHRcdFx0XHR1cGRhdGVGaWxsaW5nKCQodGhpcyksIHRpbWVsaW5lQ29tcG9uZW50c1snZmlsbGluZ0xpbmUnXSwgdGltZWxpbmVUb3RXaWR0aCk7XG5cdFx0XHRcdHVwZGF0ZVZpc2libGVDb250ZW50KCQodGhpcyksIHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzQ29udGVudCddKTtcblx0XHRcdH0pO1xuXG5cdFx0XHQvL29uIHN3aXBlLCBzaG93IG5leHQvcHJldiBldmVudCBjb250ZW50XG5cdFx0XHR0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c0NvbnRlbnQnXS5vbignc3dpcGVsZWZ0JywgZnVuY3Rpb24oKXtcblx0XHRcdFx0dmFyIG1xID0gY2hlY2tNUSgpO1xuXHRcdFx0XHQoIG1xID09ICdtb2JpbGUnICkgJiYgc2hvd05ld0NvbnRlbnQodGltZWxpbmVDb21wb25lbnRzLCB0aW1lbGluZVRvdFdpZHRoLCAnbmV4dCcpO1xuXHRcdFx0fSk7XG5cdFx0XHR0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c0NvbnRlbnQnXS5vbignc3dpcGVyaWdodCcsIGZ1bmN0aW9uKCl7XG5cdFx0XHRcdHZhciBtcSA9IGNoZWNrTVEoKTtcblx0XHRcdFx0KCBtcSA9PSAnbW9iaWxlJyApICYmIHNob3dOZXdDb250ZW50KHRpbWVsaW5lQ29tcG9uZW50cywgdGltZWxpbmVUb3RXaWR0aCwgJ3ByZXYnKTtcblx0XHRcdH0pO1xuXG5cdFx0XHQvL2tleWJvYXJkIG5hdmlnYXRpb25cblx0XHRcdCQoZG9jdW1lbnQpLmtleXVwKGZ1bmN0aW9uKGV2ZW50KXtcblx0XHRcdFx0aWYoZXZlbnQud2hpY2g9PSczNycgJiYgZWxlbWVudEluVmlld3BvcnQodGltZWxpbmUuZ2V0KDApKSApIHtcblx0XHRcdFx0XHRzaG93TmV3Q29udGVudCh0aW1lbGluZUNvbXBvbmVudHMsIHRpbWVsaW5lVG90V2lkdGgsICdwcmV2Jyk7XG5cdFx0XHRcdH0gZWxzZSBpZiggZXZlbnQud2hpY2g9PSczOScgJiYgZWxlbWVudEluVmlld3BvcnQodGltZWxpbmUuZ2V0KDApKSkge1xuXHRcdFx0XHRcdHNob3dOZXdDb250ZW50KHRpbWVsaW5lQ29tcG9uZW50cywgdGltZWxpbmVUb3RXaWR0aCwgJ25leHQnKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fSk7XG5cdH1cblxuXHRmdW5jdGlvbiB1cGRhdGVTbGlkZSh0aW1lbGluZUNvbXBvbmVudHMsIHRpbWVsaW5lVG90V2lkdGgsIHN0cmluZykge1xuXHRcdC8vcmV0cmlldmUgdHJhbnNsYXRlWCB2YWx1ZSBvZiB0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c1dyYXBwZXInXVxuXHRcdHZhciB0cmFuc2xhdGVWYWx1ZSA9IGdldFRyYW5zbGF0ZVZhbHVlKHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddKSxcblx0XHRcdHdyYXBwZXJXaWR0aCA9IE51bWJlcih0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lV3JhcHBlciddLmNzcygnd2lkdGgnKS5yZXBsYWNlKCdweCcsICcnKSk7XG5cdFx0Ly90cmFuc2xhdGUgdGhlIHRpbWVsaW5lIHRvIHRoZSBsZWZ0KCduZXh0JykvcmlnaHQoJ3ByZXYnKSBcblx0XHQoc3RyaW5nID09ICduZXh0JykgXG5cdFx0XHQ/IHRyYW5zbGF0ZVRpbWVsaW5lKHRpbWVsaW5lQ29tcG9uZW50cywgdHJhbnNsYXRlVmFsdWUgLSB3cmFwcGVyV2lkdGggKyBldmVudHNNaW5EaXN0YW5jZSwgd3JhcHBlcldpZHRoIC0gdGltZWxpbmVUb3RXaWR0aClcblx0XHRcdDogdHJhbnNsYXRlVGltZWxpbmUodGltZWxpbmVDb21wb25lbnRzLCB0cmFuc2xhdGVWYWx1ZSArIHdyYXBwZXJXaWR0aCAtIGV2ZW50c01pbkRpc3RhbmNlKTtcblx0fVxuXG5cdGZ1bmN0aW9uIHNob3dOZXdDb250ZW50KHRpbWVsaW5lQ29tcG9uZW50cywgdGltZWxpbmVUb3RXaWR0aCwgc3RyaW5nKSB7XG5cdFx0Ly9nbyBmcm9tIG9uZSBldmVudCB0byB0aGUgbmV4dC9wcmV2aW91cyBvbmVcblx0XHR2YXIgdmlzaWJsZUNvbnRlbnQgPSAgdGltZWxpbmVDb21wb25lbnRzWydldmVudHNDb250ZW50J10uZmluZCgnLnNlbGVjdGVkJyksXG5cdFx0XHRuZXdDb250ZW50ID0gKCBzdHJpbmcgPT0gJ25leHQnICkgPyB2aXNpYmxlQ29udGVudC5uZXh0KCkgOiB2aXNpYmxlQ29udGVudC5wcmV2KCk7XG5cblx0XHRpZiAoIG5ld0NvbnRlbnQubGVuZ3RoID4gMCApIHsgLy9pZiB0aGVyZSdzIGEgbmV4dC9wcmV2IGV2ZW50IC0gc2hvdyBpdFxuXHRcdFx0dmFyIHNlbGVjdGVkRGF0ZSA9IHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddLmZpbmQoJy5zZWxlY3RlZCcpLFxuXHRcdFx0XHRuZXdFdmVudCA9ICggc3RyaW5nID09ICduZXh0JyApID8gc2VsZWN0ZWREYXRlLnBhcmVudCgnbGknKS5uZXh0KCdsaScpLmNoaWxkcmVuKCdhJykgOiBzZWxlY3RlZERhdGUucGFyZW50KCdsaScpLnByZXYoJ2xpJykuY2hpbGRyZW4oJ2EnKTtcblx0XHRcdFxuXHRcdFx0dXBkYXRlRmlsbGluZyhuZXdFdmVudCwgdGltZWxpbmVDb21wb25lbnRzWydmaWxsaW5nTGluZSddLCB0aW1lbGluZVRvdFdpZHRoKTtcblx0XHRcdHVwZGF0ZVZpc2libGVDb250ZW50KG5ld0V2ZW50LCB0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c0NvbnRlbnQnXSk7XG5cdFx0XHRuZXdFdmVudC5hZGRDbGFzcygnc2VsZWN0ZWQnKTtcblx0XHRcdHNlbGVjdGVkRGF0ZS5yZW1vdmVDbGFzcygnc2VsZWN0ZWQnKTtcblx0XHRcdHVwZGF0ZU9sZGVyRXZlbnRzKG5ld0V2ZW50KTtcblx0XHRcdHVwZGF0ZVRpbWVsaW5lUG9zaXRpb24oc3RyaW5nLCBuZXdFdmVudCwgdGltZWxpbmVDb21wb25lbnRzKTtcblx0XHR9XG5cdH1cblxuXHRmdW5jdGlvbiB1cGRhdGVUaW1lbGluZVBvc2l0aW9uKHN0cmluZywgZXZlbnQsIHRpbWVsaW5lQ29tcG9uZW50cykge1xuXHRcdC8vdHJhbnNsYXRlIHRpbWVsaW5lIHRvIHRoZSBsZWZ0L3JpZ2h0IGFjY29yZGluZyB0byB0aGUgcG9zaXRpb24gb2YgdGhlIHNlbGVjdGVkIGV2ZW50XG5cdFx0dmFyIGV2ZW50U3R5bGUgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShldmVudC5nZXQoMCksIG51bGwpLFxuXHRcdFx0ZXZlbnRMZWZ0ID0gTnVtYmVyKGV2ZW50U3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcImxlZnRcIikucmVwbGFjZSgncHgnLCAnJykpLFxuXHRcdFx0dGltZWxpbmVXaWR0aCA9IE51bWJlcih0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lV3JhcHBlciddLmNzcygnd2lkdGgnKS5yZXBsYWNlKCdweCcsICcnKSksXG5cdFx0XHR0aW1lbGluZVRvdFdpZHRoID0gTnVtYmVyKHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddLmNzcygnd2lkdGgnKS5yZXBsYWNlKCdweCcsICcnKSk7XG5cdFx0dmFyIHRpbWVsaW5lVHJhbnNsYXRlID0gZ2V0VHJhbnNsYXRlVmFsdWUodGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10pO1xuXG4gICAgICAgIGlmKCAoc3RyaW5nID09ICduZXh0JyAmJiBldmVudExlZnQgPiB0aW1lbGluZVdpZHRoIC0gdGltZWxpbmVUcmFuc2xhdGUpIHx8IChzdHJpbmcgPT0gJ3ByZXYnICYmIGV2ZW50TGVmdCA8IC0gdGltZWxpbmVUcmFuc2xhdGUpICkge1xuICAgICAgICBcdHRyYW5zbGF0ZVRpbWVsaW5lKHRpbWVsaW5lQ29tcG9uZW50cywgLSBldmVudExlZnQgKyB0aW1lbGluZVdpZHRoLzIsIHRpbWVsaW5lV2lkdGggLSB0aW1lbGluZVRvdFdpZHRoKTtcbiAgICAgICAgfVxuXHR9XG5cblx0ZnVuY3Rpb24gdHJhbnNsYXRlVGltZWxpbmUodGltZWxpbmVDb21wb25lbnRzLCB2YWx1ZSwgdG90V2lkdGgpIHtcblx0XHR2YXIgZXZlbnRzV3JhcHBlciA9IHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddLmdldCgwKTtcblx0XHR2YWx1ZSA9ICh2YWx1ZSA+IDApID8gMCA6IHZhbHVlOyAvL29ubHkgbmVnYXRpdmUgdHJhbnNsYXRlIHZhbHVlXG5cdFx0dmFsdWUgPSAoICEodHlwZW9mIHRvdFdpZHRoID09PSAndW5kZWZpbmVkJykgJiYgIHZhbHVlIDwgdG90V2lkdGggKSA/IHRvdFdpZHRoIDogdmFsdWU7IC8vZG8gbm90IHRyYW5zbGF0ZSBtb3JlIHRoYW4gdGltZWxpbmUgd2lkdGhcblx0XHRzZXRUcmFuc2Zvcm1WYWx1ZShldmVudHNXcmFwcGVyLCAndHJhbnNsYXRlWCcsIHZhbHVlKydweCcpO1xuXHRcdC8vdXBkYXRlIG5hdmlnYXRpb24gYXJyb3dzIHZpc2liaWxpdHlcblx0XHQodmFsdWUgPT0gMCApID8gdGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXS5maW5kKCcucHJldicpLmFkZENsYXNzKCdpbmFjdGl2ZScpIDogdGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXS5maW5kKCcucHJldicpLnJlbW92ZUNsYXNzKCdpbmFjdGl2ZScpO1xuXHRcdCh2YWx1ZSA9PSB0b3RXaWR0aCApID8gdGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXS5maW5kKCcubmV4dCcpLmFkZENsYXNzKCdpbmFjdGl2ZScpIDogdGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZU5hdmlnYXRpb24nXS5maW5kKCcubmV4dCcpLnJlbW92ZUNsYXNzKCdpbmFjdGl2ZScpO1xuXHR9XG5cblx0ZnVuY3Rpb24gdXBkYXRlRmlsbGluZyhzZWxlY3RlZEV2ZW50LCBmaWxsaW5nLCB0b3RXaWR0aCkge1xuXHRcdC8vY2hhbmdlIC5maWxsaW5nLWxpbmUgbGVuZ3RoIGFjY29yZGluZyB0byB0aGUgc2VsZWN0ZWQgZXZlbnRcblx0XHR2YXIgZXZlbnRTdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKHNlbGVjdGVkRXZlbnQuZ2V0KDApLCBudWxsKSxcblx0XHRcdGV2ZW50TGVmdCA9IGV2ZW50U3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcImxlZnRcIiksXG5cdFx0XHRldmVudFdpZHRoID0gZXZlbnRTdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKFwid2lkdGhcIik7XG5cdFx0ZXZlbnRMZWZ0ID0gTnVtYmVyKGV2ZW50TGVmdC5yZXBsYWNlKCdweCcsICcnKSkgKyBOdW1iZXIoZXZlbnRXaWR0aC5yZXBsYWNlKCdweCcsICcnKSkvMjtcblx0XHR2YXIgc2NhbGVWYWx1ZSA9IGV2ZW50TGVmdC90b3RXaWR0aDtcblx0XHRzZXRUcmFuc2Zvcm1WYWx1ZShmaWxsaW5nLmdldCgwKSwgJ3NjYWxlWCcsIHNjYWxlVmFsdWUpO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0RGF0ZVBvc2l0aW9uKHRpbWVsaW5lQ29tcG9uZW50cywgbWluKSB7XG5cdFx0Zm9yIChpID0gMDsgaSA8IHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddLmxlbmd0aDsgaSsrKSB7IFxuXHRcdCAgICB2YXIgZGlzdGFuY2UgPSBkYXlkaWZmKHRpbWVsaW5lQ29tcG9uZW50c1sndGltZWxpbmVEYXRlcyddWzBdLCB0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRGF0ZXMnXVtpXSksXG5cdFx0ICAgIFx0ZGlzdGFuY2VOb3JtID0gTWF0aC5yb3VuZChkaXN0YW5jZS90aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c01pbkxhcHNlJ10pICsgMjtcblx0XHQgICAgdGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZUV2ZW50cyddLmVxKGkpLmNzcygnbGVmdCcsIGRpc3RhbmNlTm9ybSptaW4rJ3B4Jyk7XG5cdFx0fVxuXHR9XG5cblx0ZnVuY3Rpb24gc2V0VGltZWxpbmVXaWR0aCh0aW1lbGluZUNvbXBvbmVudHMsIHdpZHRoKSB7XG5cdFx0dmFyIHRpbWVTcGFuID0gZGF5ZGlmZih0aW1lbGluZUNvbXBvbmVudHNbJ3RpbWVsaW5lRGF0ZXMnXVswXSwgdGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZURhdGVzJ11bdGltZWxpbmVDb21wb25lbnRzWyd0aW1lbGluZURhdGVzJ10ubGVuZ3RoLTFdKSxcblx0XHRcdHRpbWVTcGFuTm9ybSA9IHRpbWVTcGFuL3RpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzTWluTGFwc2UnXSxcblx0XHRcdHRpbWVTcGFuTm9ybSA9IE1hdGgucm91bmQodGltZVNwYW5Ob3JtKSArIDQsXG5cdFx0XHR0b3RhbFdpZHRoID0gdGltZVNwYW5Ob3JtKndpZHRoO1xuXHRcdHRpbWVsaW5lQ29tcG9uZW50c1snZXZlbnRzV3JhcHBlciddLmNzcygnd2lkdGgnLCB0b3RhbFdpZHRoKydweCcpO1xuXHRcdHVwZGF0ZUZpbGxpbmcodGltZWxpbmVDb21wb25lbnRzWydldmVudHNXcmFwcGVyJ10uZmluZCgnYS5zZWxlY3RlZCcpLCB0aW1lbGluZUNvbXBvbmVudHNbJ2ZpbGxpbmdMaW5lJ10sIHRvdGFsV2lkdGgpO1xuXHRcdHVwZGF0ZVRpbWVsaW5lUG9zaXRpb24oJ25leHQnLCB0aW1lbGluZUNvbXBvbmVudHNbJ2V2ZW50c1dyYXBwZXInXS5maW5kKCdhLnNlbGVjdGVkJyksIHRpbWVsaW5lQ29tcG9uZW50cyk7XG5cdFxuXHRcdHJldHVybiB0b3RhbFdpZHRoO1xuXHR9XG5cblx0ZnVuY3Rpb24gdXBkYXRlVmlzaWJsZUNvbnRlbnQoZXZlbnQsIGV2ZW50c0NvbnRlbnQpIHtcblx0XHR2YXIgZXZlbnREYXRlID0gZXZlbnQuZGF0YSgnZGF0ZScpLFxuXHRcdFx0dmlzaWJsZUNvbnRlbnQgPSBldmVudHNDb250ZW50LmZpbmQoJy5zZWxlY3RlZCcpLFxuXHRcdFx0c2VsZWN0ZWRDb250ZW50ID0gZXZlbnRzQ29udGVudC5maW5kKCdbZGF0YS1kYXRlPVwiJysgZXZlbnREYXRlICsnXCJdJyksXG5cdFx0XHRzZWxlY3RlZENvbnRlbnRIZWlnaHQgPSBzZWxlY3RlZENvbnRlbnQuaGVpZ2h0KCk7XG5cblx0XHRpZiAoc2VsZWN0ZWRDb250ZW50LmluZGV4KCkgPiB2aXNpYmxlQ29udGVudC5pbmRleCgpKSB7XG5cdFx0XHR2YXIgY2xhc3NFbmV0ZXJpbmcgPSAnc2VsZWN0ZWQgZW50ZXItcmlnaHQnLFxuXHRcdFx0XHRjbGFzc0xlYXZpbmcgPSAnbGVhdmUtbGVmdCc7XG5cdFx0fSBlbHNlIHtcblx0XHRcdHZhciBjbGFzc0VuZXRlcmluZyA9ICdzZWxlY3RlZCBlbnRlci1sZWZ0Jyxcblx0XHRcdFx0Y2xhc3NMZWF2aW5nID0gJ2xlYXZlLXJpZ2h0Jztcblx0XHR9XG5cblx0XHRzZWxlY3RlZENvbnRlbnQuYXR0cignY2xhc3MnLCBjbGFzc0VuZXRlcmluZyk7XG5cdFx0dmlzaWJsZUNvbnRlbnQuYXR0cignY2xhc3MnLCBjbGFzc0xlYXZpbmcpLm9uZSgnd2Via2l0QW5pbWF0aW9uRW5kIG9hbmltYXRpb25lbmQgbXNBbmltYXRpb25FbmQgYW5pbWF0aW9uZW5kJywgZnVuY3Rpb24oKXtcblx0XHRcdHZpc2libGVDb250ZW50LnJlbW92ZUNsYXNzKCdsZWF2ZS1yaWdodCBsZWF2ZS1sZWZ0Jyk7XG5cdFx0XHRzZWxlY3RlZENvbnRlbnQucmVtb3ZlQ2xhc3MoJ2VudGVyLWxlZnQgZW50ZXItcmlnaHQnKTtcblx0XHR9KTtcblx0XHRldmVudHNDb250ZW50LmNzcygnaGVpZ2h0Jywgc2VsZWN0ZWRDb250ZW50SGVpZ2h0KydweCcpO1xuXHR9XG5cblx0ZnVuY3Rpb24gdXBkYXRlT2xkZXJFdmVudHMoZXZlbnQpIHtcblx0XHRldmVudC5wYXJlbnQoJ2xpJykucHJldkFsbCgnbGknKS5jaGlsZHJlbignYScpLmFkZENsYXNzKCdvbGRlci1ldmVudCcpLmVuZCgpLmVuZCgpLm5leHRBbGwoJ2xpJykuY2hpbGRyZW4oJ2EnKS5yZW1vdmVDbGFzcygnb2xkZXItZXZlbnQnKTtcblx0fVxuXG5cdGZ1bmN0aW9uIGdldFRyYW5zbGF0ZVZhbHVlKHRpbWVsaW5lKSB7XG5cdFx0dmFyIHRpbWVsaW5lU3R5bGUgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZSh0aW1lbGluZS5nZXQoMCksIG51bGwpLFxuXHRcdFx0dGltZWxpbmVUcmFuc2xhdGUgPSB0aW1lbGluZVN0eWxlLmdldFByb3BlcnR5VmFsdWUoXCItd2Via2l0LXRyYW5zZm9ybVwiKSB8fFxuICAgICAgICAgXHRcdHRpbWVsaW5lU3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcIi1tb3otdHJhbnNmb3JtXCIpIHx8XG4gICAgICAgICBcdFx0dGltZWxpbmVTdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKFwiLW1zLXRyYW5zZm9ybVwiKSB8fFxuICAgICAgICAgXHRcdHRpbWVsaW5lU3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcIi1vLXRyYW5zZm9ybVwiKSB8fFxuICAgICAgICAgXHRcdHRpbWVsaW5lU3R5bGUuZ2V0UHJvcGVydHlWYWx1ZShcInRyYW5zZm9ybVwiKTtcblxuICAgICAgICBpZiggdGltZWxpbmVUcmFuc2xhdGUuaW5kZXhPZignKCcpID49MCApIHtcbiAgICAgICAgXHR2YXIgdGltZWxpbmVUcmFuc2xhdGUgPSB0aW1lbGluZVRyYW5zbGF0ZS5zcGxpdCgnKCcpWzFdO1xuICAgIFx0XHR0aW1lbGluZVRyYW5zbGF0ZSA9IHRpbWVsaW5lVHJhbnNsYXRlLnNwbGl0KCcpJylbMF07XG4gICAgXHRcdHRpbWVsaW5lVHJhbnNsYXRlID0gdGltZWxpbmVUcmFuc2xhdGUuc3BsaXQoJywnKTtcbiAgICBcdFx0dmFyIHRyYW5zbGF0ZVZhbHVlID0gdGltZWxpbmVUcmFuc2xhdGVbNF07XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgIFx0dmFyIHRyYW5zbGF0ZVZhbHVlID0gMDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBOdW1iZXIodHJhbnNsYXRlVmFsdWUpO1xuXHR9XG5cblx0ZnVuY3Rpb24gc2V0VHJhbnNmb3JtVmFsdWUoZWxlbWVudCwgcHJvcGVydHksIHZhbHVlKSB7XG5cdFx0ZWxlbWVudC5zdHlsZVtcIi13ZWJraXQtdHJhbnNmb3JtXCJdID0gcHJvcGVydHkrXCIoXCIrdmFsdWUrXCIpXCI7XG5cdFx0ZWxlbWVudC5zdHlsZVtcIi1tb3otdHJhbnNmb3JtXCJdID0gcHJvcGVydHkrXCIoXCIrdmFsdWUrXCIpXCI7XG5cdFx0ZWxlbWVudC5zdHlsZVtcIi1tcy10cmFuc2Zvcm1cIl0gPSBwcm9wZXJ0eStcIihcIit2YWx1ZStcIilcIjtcblx0XHRlbGVtZW50LnN0eWxlW1wiLW8tdHJhbnNmb3JtXCJdID0gcHJvcGVydHkrXCIoXCIrdmFsdWUrXCIpXCI7XG5cdFx0ZWxlbWVudC5zdHlsZVtcInRyYW5zZm9ybVwiXSA9IHByb3BlcnR5K1wiKFwiK3ZhbHVlK1wiKVwiO1xuXHR9XG5cblx0Ly9iYXNlZCBvbiBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzU0MjkzOC9ob3ctZG8taS1nZXQtdGhlLW51bWJlci1vZi1kYXlzLWJldHdlZW4tdHdvLWRhdGVzLWluLWphdmFzY3JpcHRcblx0ZnVuY3Rpb24gcGFyc2VEYXRlKGV2ZW50cykge1xuXHRcdHZhciBkYXRlQXJyYXlzID0gW107XG5cdFx0ZXZlbnRzLmVhY2goZnVuY3Rpb24oKXtcblx0XHRcdHZhciBzaW5nbGVEYXRlID0gJCh0aGlzKSxcblx0XHRcdFx0ZGF0ZUNvbXAgPSBzaW5nbGVEYXRlLmRhdGEoJ2RhdGUnKS5zcGxpdCgnVCcpO1xuXHRcdFx0aWYoIGRhdGVDb21wLmxlbmd0aCA+IDEgKSB7IC8vYm90aCBERC9NTS9ZRUFSIGFuZCB0aW1lIGFyZSBwcm92aWRlZFxuXHRcdFx0XHR2YXIgZGF5Q29tcCA9IGRhdGVDb21wWzBdLnNwbGl0KCcvJyksXG5cdFx0XHRcdFx0dGltZUNvbXAgPSBkYXRlQ29tcFsxXS5zcGxpdCgnOicpO1xuXHRcdFx0fSBlbHNlIGlmKCBkYXRlQ29tcFswXS5pbmRleE9mKCc6JykgPj0wICkgeyAvL29ubHkgdGltZSBpcyBwcm92aWRlXG5cdFx0XHRcdHZhciBkYXlDb21wID0gW1wiMjAwMFwiLCBcIjBcIiwgXCIwXCJdLFxuXHRcdFx0XHRcdHRpbWVDb21wID0gZGF0ZUNvbXBbMF0uc3BsaXQoJzonKTtcblx0XHRcdH0gZWxzZSB7IC8vb25seSBERC9NTS9ZRUFSXG5cdFx0XHRcdHZhciBkYXlDb21wID0gZGF0ZUNvbXBbMF0uc3BsaXQoJy8nKSxcblx0XHRcdFx0XHR0aW1lQ29tcCA9IFtcIjBcIiwgXCIwXCJdO1xuXHRcdFx0fVxuXHRcdFx0dmFyXHRuZXdEYXRlID0gbmV3IERhdGUoZGF5Q29tcFsyXSwgZGF5Q29tcFsxXS0xLCBkYXlDb21wWzBdLCB0aW1lQ29tcFswXSwgdGltZUNvbXBbMV0pO1xuXHRcdFx0ZGF0ZUFycmF5cy5wdXNoKG5ld0RhdGUpO1xuXHRcdH0pO1xuXHQgICAgcmV0dXJuIGRhdGVBcnJheXM7XG5cdH1cblxuXHRmdW5jdGlvbiBkYXlkaWZmKGZpcnN0LCBzZWNvbmQpIHtcblx0ICAgIHJldHVybiBNYXRoLnJvdW5kKChzZWNvbmQtZmlyc3QpKTtcblx0fVxuXG5cdGZ1bmN0aW9uIG1pbkxhcHNlKGRhdGVzKSB7XG5cdFx0Ly9kZXRlcm1pbmUgdGhlIG1pbmltdW0gZGlzdGFuY2UgYW1vbmcgZXZlbnRzXG5cdFx0dmFyIGRhdGVEaXN0YW5jZXMgPSBbXTtcblx0XHRmb3IgKGkgPSAxOyBpIDwgZGF0ZXMubGVuZ3RoOyBpKyspIHsgXG5cdFx0ICAgIHZhciBkaXN0YW5jZSA9IGRheWRpZmYoZGF0ZXNbaS0xXSwgZGF0ZXNbaV0pO1xuXHRcdCAgICBkYXRlRGlzdGFuY2VzLnB1c2goZGlzdGFuY2UpO1xuXHRcdH1cblx0XHRyZXR1cm4gTWF0aC5taW4uYXBwbHkobnVsbCwgZGF0ZURpc3RhbmNlcyk7XG5cdH1cblxuXHQvKlxuXHRcdEhvdyB0byB0ZWxsIGlmIGEgRE9NIGVsZW1lbnQgaXMgdmlzaWJsZSBpbiB0aGUgY3VycmVudCB2aWV3cG9ydD9cblx0XHRodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzEyMzk5OS9ob3ctdG8tdGVsbC1pZi1hLWRvbS1lbGVtZW50LWlzLXZpc2libGUtaW4tdGhlLWN1cnJlbnQtdmlld3BvcnRcblx0Ki9cblx0ZnVuY3Rpb24gZWxlbWVudEluVmlld3BvcnQoZWwpIHtcblx0XHR2YXIgdG9wID0gZWwub2Zmc2V0VG9wO1xuXHRcdHZhciBsZWZ0ID0gZWwub2Zmc2V0TGVmdDtcblx0XHR2YXIgd2lkdGggPSBlbC5vZmZzZXRXaWR0aDtcblx0XHR2YXIgaGVpZ2h0ID0gZWwub2Zmc2V0SGVpZ2h0O1xuXG5cdFx0d2hpbGUoZWwub2Zmc2V0UGFyZW50KSB7XG5cdFx0ICAgIGVsID0gZWwub2Zmc2V0UGFyZW50O1xuXHRcdCAgICB0b3AgKz0gZWwub2Zmc2V0VG9wO1xuXHRcdCAgICBsZWZ0ICs9IGVsLm9mZnNldExlZnQ7XG5cdFx0fVxuXG5cdFx0cmV0dXJuICggICAgXG5cdFx0ICAgIHRvcCA8ICh3aW5kb3cucGFnZVlPZmZzZXQgKyB3aW5kb3cuaW5uZXJIZWlnaHQpICYmXG5cdFx0ICAgIGxlZnQgPCAod2luZG93LnBhZ2VYT2Zmc2V0ICsgd2luZG93LmlubmVyV2lkdGgpICYmXG5cdFx0ICAgICh0b3AgKyBoZWlnaHQpID4gd2luZG93LnBhZ2VZT2Zmc2V0ICYmXG5cdFx0ICAgIChsZWZ0ICsgd2lkdGgpID4gd2luZG93LnBhZ2VYT2Zmc2V0XG5cdFx0KTtcblx0fVxuXG5cdGZ1bmN0aW9uIGNoZWNrTVEoKSB7XG5cdFx0Ly9jaGVjayBpZiBtb2JpbGUgb3IgZGVza3RvcCBkZXZpY2Vcblx0XHRyZXR1cm4gd2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmNkLWhvcml6b250YWwtdGltZWxpbmUnKSwgJzo6YmVmb3JlJykuZ2V0UHJvcGVydHlWYWx1ZSgnY29udGVudCcpLnJlcGxhY2UoLycvZywgXCJcIikucmVwbGFjZSgvXCIvZywgXCJcIik7XG4gICAgfVxufSk7IFxuIiwiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIl0sInNvdXJjZVJvb3QiOiIifQ==