<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* contactsimplon.html.twig */
class __TwigTemplate_2943879393d514562746e5ffdc3c8e706120fac3ebadeafcdb5ba089c66fef55 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contactsimplon.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "contactsimplon.html.twig"));

        // line 1
        echo "<div class=\"container\">
            <img src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/cover-contact.jpg"), "html", null, true);
        echo "\" class=\"justify-content-center\" width=\"100%\" height=\"500px\">

            <div style=\"margin:1em;\"></div>

            <div style=\"padding-left: 2em;\">
                <h1 style=\"color: #ce0033;\">Contactez-nous </h1>
                <h3>Une question, une suggestion, un project, une envie?</h3>
                <div style=\"margin:1em;\"></div>


                <form method=\"GET\" action=\"\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio1\" value=\"option1\">
                        <label class=\"form-check-label\" for=\"inlineRadio1\">Mr</label>
                    </div>
                    <div class=\"form-check form-check-inline\" style=\"padding-left: 2em;\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio2\" value=\"option2\">
                        <label class=\"form-check-label\" for=\"inlineRadio2\">Mme</label>
                    </div>
                    <div class=\"form-check form-check-inline\" style=\"padding-left: 2em;\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio2\" value=\"option2\">
                        <label class=\"form-check-label\" for=\"inlineRadio2\">Autre</label>
                    </div>
                    <div style=\"margin:2em;\"></div>

                    <div class=\"row\">
                        <div class=\"col-sm-4\">
                          <div><label for=\"nom\">Nom</label></div>
                          <div><input class=\"form-input\" type=\"text\" name=\"nom\" id=\"nom\" placeholder=\"Quel est votre nom\" size=\"15em\" required></div>
                        </div>
                        <div class=\"col-sm-4\">
                            <div><label for=\"prenom\">Prenom</label></div>
                            <div><input class=\"form-input\" type=\"text\" name=\"prenom\" id=\"prenom\" placeholder=\"Quel est votre prenom\" size=\"15em\" required></div>
                        </div>
                        <div class=\"col-sm-4\">
                            <div><label for=\"email\">Email</label></div>
                            <div><input class=\"form-input\" type=\"email\" name=\"email\" size=\"15em\" required></div>
                        </div>
                    </div>
                    <div style=\"margin:2em;\"></div>

                    <div><label for=\"message\">Message</label></div>
                    <div><textarea class=\"textarea\" name=\"message\" id=\"\" placeholder=\"Enter Message\" style=\"width: 100%; height: 10em;\"></textarea required></div>
                    
                    <div class=\"margin\"></div>
                    <!--<div class=\"text-right\"><input type=\"submit\" value=\"Envoyer\"></div>-->
                    <div class=\"text-right\">
                        <a href=\"#\" class=\"btn btn-outline-danger btn__style hvr-grow-shadow\">Envoyer</a>
                    </div>
                    

                </form>

                
            </div>
            
        </div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "contactsimplon.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container\">
            <img src=\"{{ asset('build/cover-contact.jpg') }}\" class=\"justify-content-center\" width=\"100%\" height=\"500px\">

            <div style=\"margin:1em;\"></div>

            <div style=\"padding-left: 2em;\">
                <h1 style=\"color: #ce0033;\">Contactez-nous </h1>
                <h3>Une question, une suggestion, un project, une envie?</h3>
                <div style=\"margin:1em;\"></div>


                <form method=\"GET\" action=\"\">
                    <div class=\"form-check form-check-inline\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio1\" value=\"option1\">
                        <label class=\"form-check-label\" for=\"inlineRadio1\">Mr</label>
                    </div>
                    <div class=\"form-check form-check-inline\" style=\"padding-left: 2em;\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio2\" value=\"option2\">
                        <label class=\"form-check-label\" for=\"inlineRadio2\">Mme</label>
                    </div>
                    <div class=\"form-check form-check-inline\" style=\"padding-left: 2em;\">
                        <input class=\"form-check-input\" type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio2\" value=\"option2\">
                        <label class=\"form-check-label\" for=\"inlineRadio2\">Autre</label>
                    </div>
                    <div style=\"margin:2em;\"></div>

                    <div class=\"row\">
                        <div class=\"col-sm-4\">
                          <div><label for=\"nom\">Nom</label></div>
                          <div><input class=\"form-input\" type=\"text\" name=\"nom\" id=\"nom\" placeholder=\"Quel est votre nom\" size=\"15em\" required></div>
                        </div>
                        <div class=\"col-sm-4\">
                            <div><label for=\"prenom\">Prenom</label></div>
                            <div><input class=\"form-input\" type=\"text\" name=\"prenom\" id=\"prenom\" placeholder=\"Quel est votre prenom\" size=\"15em\" required></div>
                        </div>
                        <div class=\"col-sm-4\">
                            <div><label for=\"email\">Email</label></div>
                            <div><input class=\"form-input\" type=\"email\" name=\"email\" size=\"15em\" required></div>
                        </div>
                    </div>
                    <div style=\"margin:2em;\"></div>

                    <div><label for=\"message\">Message</label></div>
                    <div><textarea class=\"textarea\" name=\"message\" id=\"\" placeholder=\"Enter Message\" style=\"width: 100%; height: 10em;\"></textarea required></div>
                    
                    <div class=\"margin\"></div>
                    <!--<div class=\"text-right\"><input type=\"submit\" value=\"Envoyer\"></div>-->
                    <div class=\"text-right\">
                        <a href=\"#\" class=\"btn btn-outline-danger btn__style hvr-grow-shadow\">Envoyer</a>
                    </div>
                    

                </form>

                
            </div>
            
        </div>", "contactsimplon.html.twig", "/home/malhafez/symfony/simplon/templates/contactsimplon.html.twig");
    }
}
