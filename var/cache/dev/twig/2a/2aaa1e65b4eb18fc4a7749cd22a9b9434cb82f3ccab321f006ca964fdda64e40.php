<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* navbar.html.twig */
class __TwigTemplate_4eaa2677a8834585148e04e4705c22fc3a78ae602cb5d643cd65bd84c248d5ac extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "navbar.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "navbar.html.twig"));

        // line 1
        echo "<Header>
                    <div class=\"container\">
                        <nav class=\"navbar navbar-expand-lg navbar-light bg-white\">
                            <div class=\"container\">
                                <a class=\"navbar-brand\" href=\"#\">
                                    <img src=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/index1.png"), "html", null, true);
        echo "\" class=\"d-inline-block\" title=\"Logo Bookmark\" alt=\"Logo Bookmark\" width=\"85%\">
                                </a>
                                <button type=\"button\" class=\"navbar-toggler\" data-toggle=\"modal\" data-target=\"#navbarToggler\">
                                    <img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/open-menu.svg"), "html", null, true);
        echo "\" alt=\"more\" title=\"More\" width=\"20px\" height=\"20px\">
                                </button>
                                <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\" style=\"position: relative; margin-left: 30em\">
                                    <ul class=\"navbar-nav\">
                                    
                                    <block style=\"color: red; position: relative; top: 1px; left: -12px;\">|</block>
                                    <a class=\"form-inline\" href=\"a-propos.html\" style=\"color: black; position: relative; top: 2px; left: -3px;\">A propos</a>
                                    </ul>
                                    
                                    <block style=\"color: red; position: relative; top: 2px; left: 5px;\">|</block>
                                    
                                    <a class=\"form-inline\" href=\"indexcotact.html\" style=\"color: black; position: relative; top: 2px; left: 1em;\">Nous contacter</a>
                
                                    <block style=\"color: red; position: relative; top: 2px; left: 28px;\">|</block>
                                </div>
                            </div>
                        </nav>
                
                        <article class=\"modal fade\" id=\"navbarToggler\" tabindex=\"-1\" role=\"dialog\">
                            <div class=\"modal-dialog m-0 h-100\" role=\"document\">
                                <section class=\"modal-content m-0 pt-5 vw-100 text-center\">
                                    <header class=\"container d-flex justify-content-between mb-3\">
                                        
                                        <img src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/unnamed.jpg"), "html", null, true);
        echo "\" class=\"d-inline-block\" title=\"Logo Bookmark\" alt=\"Logo Bookmark\" width=\"150\" height=\"70\">
                                        <button type=\"button\" class=\"btn modal-button-close\" data-dismiss=\"modal\">
                                            <img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/close.svg"), "html", null, true);
        echo "\" class=\"d-inline-block align-top\" title=\"Close\" alt=\"close\">
                                        </button>
                                    </header>
                                    <section class=\"container text-center\">
                                        <div class=\"row\">
                                            
                                        <div class=\"col-12\">
                                            <hr>
                                            <a class=\"form-inline\" href=\"a-propos.html\" style=\"padding-left: 30%;\">A propos</a>
                                        </div>
                                        <div class=\"col-12\">
                                            <hr>
                                            <a class=\"form-inline\" href=\"indexcotact.html\" style=\"padding-left: 30%;\">Nous contacter</a>
                                        </div>
                                        
                                    </div>
                                </section>
                                <div class=\"text-center\">
                                    <hr>
                                    <a class=\"icon\" href=\"#\">
                                        <img class=\"mx-3\" src=\"assets/images/twitter1.svg\" alt=\"twitter\" style=\"width: 30px; height: 30px;\">
                                    </a>
            
                                    <a class=\"icon\" href=\"#\">
                                        <img class=\"mx-3\" src=\"assets/images/facebook1.svg\" alt=\"facebook\" style=\"width: 30px; height: 30px;\">
                                    </a>
            
                                    <a class=\"icon\" href=\"#\">
                                        <img class=\"mx-3\" src=\"assets/images/linkedin1.svg\" alt=\"linkedln\" style=\"width: 30px; height: 30px;\">
                                    </a>
            
                                    <a class=\"icon\" href=\"https://youtube.com/\">
                                        <img class=\"mx-3\" src=\"assets/images/youtube1.svg\" alt=\"youtube\" style=\"width: 30px; height: 30px;\">
                                    </a>
                                    
                
                                </div>
                      
                        
                            </section>
                        </div>
                    </article>
            
                </div>
                
            </Header>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 34,  82 => 32,  56 => 9,  50 => 6,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<Header>
                    <div class=\"container\">
                        <nav class=\"navbar navbar-expand-lg navbar-light bg-white\">
                            <div class=\"container\">
                                <a class=\"navbar-brand\" href=\"#\">
                                    <img src=\"{{ asset('build/index1.png') }}\" class=\"d-inline-block\" title=\"Logo Bookmark\" alt=\"Logo Bookmark\" width=\"85%\">
                                </a>
                                <button type=\"button\" class=\"navbar-toggler\" data-toggle=\"modal\" data-target=\"#navbarToggler\">
                                    <img src=\"{{ asset('build/open-menu.svg') }}\" alt=\"more\" title=\"More\" width=\"20px\" height=\"20px\">
                                </button>
                                <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\" style=\"position: relative; margin-left: 30em\">
                                    <ul class=\"navbar-nav\">
                                    
                                    <block style=\"color: red; position: relative; top: 1px; left: -12px;\">|</block>
                                    <a class=\"form-inline\" href=\"a-propos.html\" style=\"color: black; position: relative; top: 2px; left: -3px;\">A propos</a>
                                    </ul>
                                    
                                    <block style=\"color: red; position: relative; top: 2px; left: 5px;\">|</block>
                                    
                                    <a class=\"form-inline\" href=\"indexcotact.html\" style=\"color: black; position: relative; top: 2px; left: 1em;\">Nous contacter</a>
                
                                    <block style=\"color: red; position: relative; top: 2px; left: 28px;\">|</block>
                                </div>
                            </div>
                        </nav>
                
                        <article class=\"modal fade\" id=\"navbarToggler\" tabindex=\"-1\" role=\"dialog\">
                            <div class=\"modal-dialog m-0 h-100\" role=\"document\">
                                <section class=\"modal-content m-0 pt-5 vw-100 text-center\">
                                    <header class=\"container d-flex justify-content-between mb-3\">
                                        
                                        <img src=\"{{ asset('build/unnamed.jpg') }}\" class=\"d-inline-block\" title=\"Logo Bookmark\" alt=\"Logo Bookmark\" width=\"150\" height=\"70\">
                                        <button type=\"button\" class=\"btn modal-button-close\" data-dismiss=\"modal\">
                                            <img src=\"{{ asset('build/close.svg') }}\" class=\"d-inline-block align-top\" title=\"Close\" alt=\"close\">
                                        </button>
                                    </header>
                                    <section class=\"container text-center\">
                                        <div class=\"row\">
                                            
                                        <div class=\"col-12\">
                                            <hr>
                                            <a class=\"form-inline\" href=\"a-propos.html\" style=\"padding-left: 30%;\">A propos</a>
                                        </div>
                                        <div class=\"col-12\">
                                            <hr>
                                            <a class=\"form-inline\" href=\"indexcotact.html\" style=\"padding-left: 30%;\">Nous contacter</a>
                                        </div>
                                        
                                    </div>
                                </section>
                                <div class=\"text-center\">
                                    <hr>
                                    <a class=\"icon\" href=\"#\">
                                        <img class=\"mx-3\" src=\"assets/images/twitter1.svg\" alt=\"twitter\" style=\"width: 30px; height: 30px;\">
                                    </a>
            
                                    <a class=\"icon\" href=\"#\">
                                        <img class=\"mx-3\" src=\"assets/images/facebook1.svg\" alt=\"facebook\" style=\"width: 30px; height: 30px;\">
                                    </a>
            
                                    <a class=\"icon\" href=\"#\">
                                        <img class=\"mx-3\" src=\"assets/images/linkedin1.svg\" alt=\"linkedln\" style=\"width: 30px; height: 30px;\">
                                    </a>
            
                                    <a class=\"icon\" href=\"https://youtube.com/\">
                                        <img class=\"mx-3\" src=\"assets/images/youtube1.svg\" alt=\"youtube\" style=\"width: 30px; height: 30px;\">
                                    </a>
                                    
                
                                </div>
                      
                        
                            </section>
                        </div>
                    </article>
            
                </div>
                
            </Header>", "navbar.html.twig", "/home/malhafez/symfony/simplon/templates/navbar.html.twig");
    }
}
