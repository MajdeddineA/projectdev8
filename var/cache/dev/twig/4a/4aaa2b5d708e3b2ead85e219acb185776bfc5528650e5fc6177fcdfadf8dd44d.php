<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* mainplus.html.twig */
class __TwigTemplate_fca19be472b5203281264e902771cde4d10d9e4275bf76eef358712911f38908 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "mainplus.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "mainplus.html.twig"));

        // line 1
        echo "<div class=\"container img-fluid-container center-text\">
    <img style=\"position: relative; width: 600px; height: 400px;\" class=\"rounded mx-auto d-block\" src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/tb-is-t2-2020___medialibrary_original_587_367.jpg"), "html", null, true);
        echo "\">
</div>

<div class=\"container shareable content-article \">
    <p>En travaillant main dans la main avec nos partenaires en France et à l’international, nous avons déployé un réseau de <strong>99 Fabriques</strong>. Grâce à notre présence dans les différents territoires/pays, il y a actuellement <strong>5 924 apprenant.es qui sont formé.es ou en cours de formation en France, et 2 700 à l’international.</strong>&nbsp;</p> 
    <p>Aussi, grâce aux efforts de nos parties prenantes et de nos équipes, nous continuons à mettre le numérique au service de tout.es. Cette année, nous avons formé différents profils d’apprenant.es en France et à l’international. En France, ils et elles sont :</p> 
    <ul>
        <li>
            <p>32% de femmes</p></li> <li><p>84% de demandeur.ses d’emploi</p>
        </li> 
        <li>
            <p>47% de personnes avec un niveau de diplôme infra-bac ou bac (niveau 2 ou 3)&nbsp;</p>
        </li>
    </ul>
    <p>Enfin, toujours dans l’objectif de faciliter l’accès au numérique, nous initions des enfants et des jeunes au code. Les ateliers de sensibilisation du jeune public sont pour la plupart animés par les apprenant.es, et mettent en application la pédagogie du “learning by doing”. Pendant cette période, <strong>4 868 jeunes ont été sensibilisé.es en France par 212 apprenant.es mobilisé.es dans ce cadre.</strong></p> 
    <h3>Simplon à l’heure du Covid-19</h3> 
    <p>En lien avec nos valeurs et nos objectifs, nous avons décidé d’assurer la continuité de nos activités de formation pendant cette période de crise sanitaire. Les équipes de Simplon se sont mobilisées pour déployer des méthodes et des modalités de formation à distance afin d’assurer une continuité dans les parcours de nos apprenant.es. Malgré nos efforts pour continuer nos actions, nous avons vu l’impact de cette crise sanitaire dans nos activités.&nbsp;</p> 
    <h3>Nos Fabriques, mode d’emploi</h3> 
    <p>Chez Simplon, nous appelons Fabrique une école numérique inclusive qui accueille une ou plusieurs formations, parfois un Fablab et/ou des activités de production. Ces lieux se développent selon les besoins des territoires. Une Fabrique peut changer de statut -ouverte/fermée- d’un trimestre à l’autre selon l’offre de formations.&nbsp;</p> 
    <p>Pour en savoir plus, téléchargez le rapport d’impact trimestriel qui rend compte de nos actions, de nos résultats et de notre impact.</p> 
    <p><a href=\"https://simplon.co/uploads/Contenus%20articles/Rapport%20d%27impact%20T2%202020.pdf\" target=\"_blank\">Notre rapport d'impact social en français</a></p> 
    <p><a href=\"https://simplon.co/uploads/Contenus%20articles/Social%20Impact%20Dashboard%20T2%202020.pdf\" target=\"_blank\">Notre rapport d'impact social&nbsp;en anglais</a></p> 
    
</div>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "mainplus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container img-fluid-container center-text\">
    <img style=\"position: relative; width: 600px; height: 400px;\" class=\"rounded mx-auto d-block\" src=\"{{ asset('build/tb-is-t2-2020___medialibrary_original_587_367.jpg') }}\">
</div>

<div class=\"container shareable content-article \">
    <p>En travaillant main dans la main avec nos partenaires en France et à l’international, nous avons déployé un réseau de <strong>99 Fabriques</strong>. Grâce à notre présence dans les différents territoires/pays, il y a actuellement <strong>5 924 apprenant.es qui sont formé.es ou en cours de formation en France, et 2 700 à l’international.</strong>&nbsp;</p> 
    <p>Aussi, grâce aux efforts de nos parties prenantes et de nos équipes, nous continuons à mettre le numérique au service de tout.es. Cette année, nous avons formé différents profils d’apprenant.es en France et à l’international. En France, ils et elles sont :</p> 
    <ul>
        <li>
            <p>32% de femmes</p></li> <li><p>84% de demandeur.ses d’emploi</p>
        </li> 
        <li>
            <p>47% de personnes avec un niveau de diplôme infra-bac ou bac (niveau 2 ou 3)&nbsp;</p>
        </li>
    </ul>
    <p>Enfin, toujours dans l’objectif de faciliter l’accès au numérique, nous initions des enfants et des jeunes au code. Les ateliers de sensibilisation du jeune public sont pour la plupart animés par les apprenant.es, et mettent en application la pédagogie du “learning by doing”. Pendant cette période, <strong>4 868 jeunes ont été sensibilisé.es en France par 212 apprenant.es mobilisé.es dans ce cadre.</strong></p> 
    <h3>Simplon à l’heure du Covid-19</h3> 
    <p>En lien avec nos valeurs et nos objectifs, nous avons décidé d’assurer la continuité de nos activités de formation pendant cette période de crise sanitaire. Les équipes de Simplon se sont mobilisées pour déployer des méthodes et des modalités de formation à distance afin d’assurer une continuité dans les parcours de nos apprenant.es. Malgré nos efforts pour continuer nos actions, nous avons vu l’impact de cette crise sanitaire dans nos activités.&nbsp;</p> 
    <h3>Nos Fabriques, mode d’emploi</h3> 
    <p>Chez Simplon, nous appelons Fabrique une école numérique inclusive qui accueille une ou plusieurs formations, parfois un Fablab et/ou des activités de production. Ces lieux se développent selon les besoins des territoires. Une Fabrique peut changer de statut -ouverte/fermée- d’un trimestre à l’autre selon l’offre de formations.&nbsp;</p> 
    <p>Pour en savoir plus, téléchargez le rapport d’impact trimestriel qui rend compte de nos actions, de nos résultats et de notre impact.</p> 
    <p><a href=\"https://simplon.co/uploads/Contenus%20articles/Rapport%20d%27impact%20T2%202020.pdf\" target=\"_blank\">Notre rapport d'impact social en français</a></p> 
    <p><a href=\"https://simplon.co/uploads/Contenus%20articles/Social%20Impact%20Dashboard%20T2%202020.pdf\" target=\"_blank\">Notre rapport d'impact social&nbsp;en anglais</a></p> 
    
</div>", "mainplus.html.twig", "/home/malhafez/symfony/simplon/templates/mainplus.html.twig");
    }
}
