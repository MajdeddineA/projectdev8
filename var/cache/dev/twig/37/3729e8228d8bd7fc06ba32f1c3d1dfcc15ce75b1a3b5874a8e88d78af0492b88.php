<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* main.html.twig */
class __TwigTemplate_f3e0cf4dce76b902dbeb3ef5cacf9080b25282a1aaf41ab2d685c770a1b9cd66 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "main.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "main.html.twig"));

        // line 1
        echo " <main>
               
                <section class=\"container\">
                    <div class=\"text-center\">
                      
                      <h2 style=\"color: red\">Notre impact </h2>
                      
                    </div>
                
                    <div class=\"row\">
                      <div class=\"col-sm-4\">
                        
                        <div class=\"text-center\">
                          <h5 style=\"margin-bottom: 0.6em; font-size: 40px\">8624</h5>
                          <p>Simploniens dans le monde</p>
                          
                        </div>
                      </div>
                      <div class=\"col-sm-4\">
                        
                        <div class=\"text-center\">
                          <h5 style=\"margin-bottom: 0.6em; font-size: 40px\">99</h5>
                          <p>Fabriques dans le monde</p>
                          
                        </div>
                      </div>    
                      <div class=\"col-sm-4\">
                        
                        <div class=\"text-center\">
                          <h5 style=\"margin-bottom: 0.6em; font-size: 40px\">70 %</h5>
                          <p>De sorties positives après la formation</p>

                        </div>
                      </div>
                    </div>
                  </section>



                  <section class=\"container\" style=\"margin-top: 3em;\">
                    <div class=\"text-center\">
                      
                    </div>
                
                    <div class=\"row justify-content-center\">
                      <div class=\"col-md-6\">
                        <img style=\"position: relative; width: 500px; height: 300px;\" class=\"rounded mx-auto d-block\" src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/tb-is-t2-2020___medialibrary_original_587_367.jpg"), "html", null, true);
        echo "\">
                        <div class=\"text-center\">
                          <h5>Simplon a formé plus de 8 600 Simploniens en France et l’international !</h5>
                          <p>A l'heure du Covid-19, Simplon continue à déployer ses formations partout dans le monde et à former des apprenant.es aux métiers du numérique.</p>
                          <a class=\"btn btn-outline-danger btn__style hvr-grow-shadow\" href=\"/base/plus\">EN SAVOIR PLUS</a>
                        </div>
                      </div>
                      <div class=\"col-md-6\">
                        <img style=\"position: relative; width: 500px; height: 300px;\" class=\"rounded mx-auto d-block\" src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/apprentissage___medialibrary_original_599_299.png"), "html", null, true);
        echo "\">
                        <div class=\"text-center\">
                          <h5>Les CFA Simplon proposent désormais des formations en apprentissage pour les jeunes</h5>
                          <p>Simplon propose depuis janvier toutes ses formations certifiantes au travers de titres professionnels en apprentissage</p>
                          <a class=\"btn btn-outline-danger btn__style hvr-grow-shadow\" href=\"/base/pluss\">EN SAVOIR PLUS</a>
                        </div>
                      </div>
                    </div>
                  </section>

                  <section class=\"container\">
                  </section>

            </main>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 55,  91 => 47,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source(" <main>
               
                <section class=\"container\">
                    <div class=\"text-center\">
                      
                      <h2 style=\"color: red\">Notre impact </h2>
                      
                    </div>
                
                    <div class=\"row\">
                      <div class=\"col-sm-4\">
                        
                        <div class=\"text-center\">
                          <h5 style=\"margin-bottom: 0.6em; font-size: 40px\">8624</h5>
                          <p>Simploniens dans le monde</p>
                          
                        </div>
                      </div>
                      <div class=\"col-sm-4\">
                        
                        <div class=\"text-center\">
                          <h5 style=\"margin-bottom: 0.6em; font-size: 40px\">99</h5>
                          <p>Fabriques dans le monde</p>
                          
                        </div>
                      </div>    
                      <div class=\"col-sm-4\">
                        
                        <div class=\"text-center\">
                          <h5 style=\"margin-bottom: 0.6em; font-size: 40px\">70 %</h5>
                          <p>De sorties positives après la formation</p>

                        </div>
                      </div>
                    </div>
                  </section>



                  <section class=\"container\" style=\"margin-top: 3em;\">
                    <div class=\"text-center\">
                      
                    </div>
                
                    <div class=\"row justify-content-center\">
                      <div class=\"col-md-6\">
                        <img style=\"position: relative; width: 500px; height: 300px;\" class=\"rounded mx-auto d-block\" src=\"{{ asset('build/tb-is-t2-2020___medialibrary_original_587_367.jpg') }}\">
                        <div class=\"text-center\">
                          <h5>Simplon a formé plus de 8 600 Simploniens en France et l’international !</h5>
                          <p>A l'heure du Covid-19, Simplon continue à déployer ses formations partout dans le monde et à former des apprenant.es aux métiers du numérique.</p>
                          <a class=\"btn btn-outline-danger btn__style hvr-grow-shadow\" href=\"/base/plus\">EN SAVOIR PLUS</a>
                        </div>
                      </div>
                      <div class=\"col-md-6\">
                        <img style=\"position: relative; width: 500px; height: 300px;\" class=\"rounded mx-auto d-block\" src=\"{{ asset('build/apprentissage___medialibrary_original_599_299.png') }}\">
                        <div class=\"text-center\">
                          <h5>Les CFA Simplon proposent désormais des formations en apprentissage pour les jeunes</h5>
                          <p>Simplon propose depuis janvier toutes ses formations certifiantes au travers de titres professionnels en apprentissage</p>
                          <a class=\"btn btn-outline-danger btn__style hvr-grow-shadow\" href=\"/base/pluss\">EN SAVOIR PLUS</a>
                        </div>
                      </div>
                    </div>
                  </section>

                  <section class=\"container\">
                  </section>

            </main>", "main.html.twig", "/home/malhafez/symfony/simplon/templates/main.html.twig");
    }
}
