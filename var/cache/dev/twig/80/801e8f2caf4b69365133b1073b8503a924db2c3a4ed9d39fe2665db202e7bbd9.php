<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* footer.html.twig */
class __TwigTemplate_57b8ed02e27bdf5b9e176c8e6b90482f09eb34a2e016a289d426df9a93c32c29 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "footer.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "footer.html.twig"));

        // line 1
        echo "<footer class=\"footer text-center\">


                <!-- START social media -->
                <div class= \"row justify-content-center\">
      
                    <div class=\"hover08\">
                        <div><a href=\"#\"><figure><img class=\"icon__social\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/twitter.svg"), "html", null, true);
        echo "\" alt=\"twitter\" title=\"Twitter\"></figure></a>
                        </div>
                    </div>
      
                    <div class=\"hover08\">
                        <div><a href=\"#\"><figure><img class=\"icon__social \" 
                            src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/facebook.svg"), "html", null, true);
        echo "\" alt=\"facebook\" title=\"Facebook\"></figure></a>
                        </div>
                    </div>
        
                    <div class=\"hover08\">
                        <div><a href=\"#\"><figure><img class=\"icon__social\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/linkedin.svg"), "html", null, true);
        echo "\" alt=\"linkedln\" title=\"linkedln\"></figure></a>
                        </div>
                    </div>
        
                    <div class=\"hover08\">
                        <div><a href=\"#\"><figure><img class=\"icon__social\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/youtube.svg"), "html", null, true);
        echo "\" alt=\"youtube\" title=\"youtube\"></figure></a>
                        </div>
                    </div>
      
                </div>
                <!-- END social media  -->
        
        
                
                <div class=\"footer attribution row justify-content-center\">

                    <a class=\"btn px-4 py-2 mx-1\" href=\"mentions-legales.html\">Mentions légales</a>
                    <block style=\"width: 0.5em; height: 1em; position: relative; top: 5px;\">|</block>
      
                    <a class=\"btn px-4 py-2 mx-1\" href=\"anti-faq.html\">L'anti-FAQ</a>
                    <block style=\"width: 0.5em; height: 1em; position: relative; top: 5px;\">|</block>
      
                    <a class=\"btn px-4 py-2 mx-1\" href=\"/base/contact\">Contact</a>
      
                </div>
                </footer>
            </footer>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 24,  69 => 19,  61 => 14,  52 => 8,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer class=\"footer text-center\">


                <!-- START social media -->
                <div class= \"row justify-content-center\">
      
                    <div class=\"hover08\">
                        <div><a href=\"#\"><figure><img class=\"icon__social\" src=\"{{ asset('build/twitter.svg') }}\" alt=\"twitter\" title=\"Twitter\"></figure></a>
                        </div>
                    </div>
      
                    <div class=\"hover08\">
                        <div><a href=\"#\"><figure><img class=\"icon__social \" 
                            src=\"{{ asset('build/facebook.svg') }}\" alt=\"facebook\" title=\"Facebook\"></figure></a>
                        </div>
                    </div>
        
                    <div class=\"hover08\">
                        <div><a href=\"#\"><figure><img class=\"icon__social\" src=\"{{ asset('build/linkedin.svg') }}\" alt=\"linkedln\" title=\"linkedln\"></figure></a>
                        </div>
                    </div>
        
                    <div class=\"hover08\">
                        <div><a href=\"#\"><figure><img class=\"icon__social\" src=\"{{ asset('build/youtube.svg') }}\" alt=\"youtube\" title=\"youtube\"></figure></a>
                        </div>
                    </div>
      
                </div>
                <!-- END social media  -->
        
        
                
                <div class=\"footer attribution row justify-content-center\">

                    <a class=\"btn px-4 py-2 mx-1\" href=\"mentions-legales.html\">Mentions légales</a>
                    <block style=\"width: 0.5em; height: 1em; position: relative; top: 5px;\">|</block>
      
                    <a class=\"btn px-4 py-2 mx-1\" href=\"anti-faq.html\">L'anti-FAQ</a>
                    <block style=\"width: 0.5em; height: 1em; position: relative; top: 5px;\">|</block>
      
                    <a class=\"btn px-4 py-2 mx-1\" href=\"/base/contact\">Contact</a>
      
                </div>
                </footer>
            </footer>", "footer.html.twig", "/home/malhafez/symfony/simplon/templates/footer.html.twig");
    }
}
