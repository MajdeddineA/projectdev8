<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/company/new' => [[['_route' => 'new_company', '_controller' => 'App\\Controller\\CompanyController::newCompany'], null, null, null, false, false, null]],
        '/company/list' => [[['_route' => 'listCompany', '_controller' => 'App\\Controller\\CompanyController::listCompany'], null, null, null, false, false, null]],
        '/home' => [[['_route' => 'app_home_home', '_controller' => 'App\\Controller\\HomeController::home'], null, null, null, false, false, null]],
        '/base' => [[['_route' => 'app_home_base', '_controller' => 'App\\Controller\\HomeController::base'], null, null, null, false, false, null]],
        '/base/contact' => [[['_route' => 'app_home_contactsimplon', '_controller' => 'App\\Controller\\HomeController::contactsimplon'], null, null, null, false, false, null]],
        '/base/apropos' => [[['_route' => 'app_home_apropos', '_controller' => 'App\\Controller\\HomeController::apropos'], null, null, null, false, false, null]],
        '/base/plus' => [[['_route' => 'app_home_plus', '_controller' => 'App\\Controller\\HomeController::plus'], null, null, null, false, false, null]],
        '/base/pluss' => [[['_route' => 'app_home_pluss', '_controller' => 'App\\Controller\\HomeController::pluss'], null, null, null, false, false, null]],
        '/home/contact' => [[['_route' => 'app_home_contact', '_controller' => 'App\\Controller\\HomeController::contact'], null, null, null, false, false, null]],
        '/person/new' => [[['_route' => 'newPerson', '_controller' => 'App\\Controller\\PersonController::newPerson'], null, null, null, false, false, null]],
        '/person/list' => [[['_route' => 'listPerson', '_controller' => 'App\\Controller\\PersonController::listPerson'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/company/(?'
                    .'|([^/]++)(*:189)'
                    .'|edit/([^/]++)(*:210)'
                    .'|delete/([^/]++)(*:233)'
                .')'
                .'|/home/getcontact/([^/]++)(*:267)'
                .'|/p(?'
                    .'|lus(?'
                        .'|/(\\d+)/(\\d+)(*:298)'
                        .'|s/(\\d+)/(\\d+)(*:319)'
                    .')'
                    .'|erson/(?'
                        .'|([^/]++)(*:345)'
                        .'|edit/([^/]++)(*:366)'
                        .'|delete/([^/]++)(*:389)'
                    .')'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        189 => [[['_route' => 'showCompany', '_controller' => 'App\\Controller\\CompanyController::showCompany'], ['id'], null, null, false, true, null]],
        210 => [[['_route' => 'editCompany', '_controller' => 'App\\Controller\\CompanyController::editCompany'], ['id'], null, null, false, true, null]],
        233 => [[['_route' => 'deleteCompany', '_controller' => 'App\\Controller\\CompanyController::deleteCompany'], ['id'], null, null, false, true, null]],
        267 => [[['_route' => 'getcontact', '_controller' => 'App\\Controller\\HomeController::getcontact'], ['prenom'], ['GET' => 0], null, false, true, null]],
        298 => [[['_route' => 'sum', '_controller' => 'App\\Controller\\HomeController::sum'], ['a', 'b'], ['GET' => 0], null, false, true, null]],
        319 => [[['_route' => 'summ', '_controller' => 'App\\Controller\\HomeController::summ'], ['a', 'b'], ['GET' => 0], null, false, true, null]],
        345 => [[['_route' => 'showPerson', '_controller' => 'App\\Controller\\PersonController::showPerson'], ['id'], null, null, false, true, null]],
        366 => [[['_route' => 'editPerson', '_controller' => 'App\\Controller\\PersonController::editPerson'], ['id'], null, null, false, true, null]],
        389 => [
            [['_route' => 'deletePerson', '_controller' => 'App\\Controller\\PersonController::deletePerson'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
